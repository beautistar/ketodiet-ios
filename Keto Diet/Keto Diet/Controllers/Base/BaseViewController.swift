//
//  BaseViewController.swift
//  Keto Diet
//


import UIKit
import Toast_Swift


class BaseViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()        
        
    }
    
    
    internal func showAlert(title: String?, message: String?, okButtonTitle: String, cancelButtonTitle: String?, okClosure: (() -> Void)?) {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let yesAction = UIAlertAction(title: okButtonTitle, style: .default, handler: { (action: UIAlertAction) in
            
            if okClosure != nil {
                okClosure!()
            }
        })
        
        alertController.addAction(yesAction)
        if cancelButtonTitle != nil {
            
            let noAction = UIAlertAction(title: cancelButtonTitle, style: .cancel, handler: { (action: UIAlertAction) in
                
            })
            alertController.addAction(noAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    

    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showLoadingView() {
        
        showLoadingViewWithTitle(title: "")
    }
    
    func showLoadingViewWithTitle(title: String) {
        
        if title == "" {
            
            ProgressHUD.show()
            
        } else {
            
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    // hide loading view
    func hideLoadingView() {
        
        ProgressHUD.dismiss()
    }
    
    @IBAction func backAction(_ sender: Any) {

        self.navigationController?.popViewController(animated: true)
    }
    
    //Toast
    func showToast(_ text:String) {
        self.view.makeToast(text)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
}


