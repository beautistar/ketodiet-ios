//
//  ObjectiveViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Presentr

class ObjectiveViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var goalView: UIView!
    @IBOutlet weak var nextView: UIView!
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    var isSelect: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    
    func initView() {
        
        nextView.layer.borderColor = UIColor.white.cgColor
        goalView.layer.borderColor = UIColor.white.cgColor
        goalView.layer.borderWidth = 2.0
        goalView.clipsToBounds = true
        goalView.layer.cornerRadius = 10
        goalView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        user.goal = 0
        
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if !isSelect {
            self.showAlertDialog(title: Const.AppName, message: "Please select your goal", positive: "OK", negative: nil)
        } else {
            gotoIntake()
        }
        
    }
    
    func gotoIntake() {
        
        let createEventVC = self.storyboard?.instantiateViewController(withIdentifier: "DailyIntakeViewController") as! DailyIntakeViewController
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.fluid(percentage: 0.7), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: createEventVC, animated: true, completion: nil)
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ObjectiveCell") as! ObjectiveCell
        
        cell.imvObjective.image = Const.goalImages[indexPath.row]
        cell.lblTitle.text = Const.goalTitles[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        isSelect = true
        user.goal = indexPath.row
    }
}
