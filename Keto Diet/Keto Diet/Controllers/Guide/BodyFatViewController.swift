//
//  BodyFatViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class BodyFatViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    

    @IBOutlet weak var fatView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var cvBodyFat: UICollectionView!    
    @IBOutlet weak var tfBodyFat: UITextField!
    
    var collectionViewWidth: CGFloat = 0.0
    var selectedStats = [false, false, false, false, false, false, false, false, false]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        initView()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    
    func initView() {
        
        nextView.layer.borderColor = UIColor.white.cgColor
        fatView.layer.borderColor = UIColor.white.cgColor
        fatView.layer.borderWidth = 2.0
        fatView.clipsToBounds = true
        fatView.layer.cornerRadius = 10
        fatView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        collectionViewWidth = self.view.bounds.width - 40
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if tfBodyFat.text?.count == 0 {
            self.showAlertDialog(title: Const.AppName, message: "Enter your body fat", positive: "OK", negative: nil)
        } else {
            user.fat = Float(tfBodyFat.text!)!
            gotoSelectActivity()
        }
    }
    
    func gotoSelectActivity() {
        let activityVC = self.storyboard?.instantiateViewController(withIdentifier: "ActivityViewController") as! ActivityViewController
        self.navigationController?.pushViewController(activityVC, animated: true)
    }
    
    //MARK: - CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BodyFatCell", for: indexPath) as! BodyFatCell
        if user.gender == Const.MALE {
            cell.imvBody.image = Const.maleBodyFatImages[indexPath.row]
            cell.lblFat.text = Const.maleBodyFats[indexPath.row]
        } else {
            cell.imvBody.image = Const.femaleBodyFatImages[indexPath.row]
            cell.lblFat.text = Const.femaleBodyFats[indexPath.row]
        }
        
        if selectedStats[indexPath.row] {
            cell.borderView.isHidden = false
        } else {
            cell.borderView.isHidden = true
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if user.gender == Const.MALE {
            tfBodyFat.text = "\(Const.maleBodyFatValues[indexPath.row])"
        } else {
            tfBodyFat.text = "\(Const.femaleBodyFatValues[indexPath.row])"
        }
        
        for index in 0...selectedStats.count-1 {
            selectedStats[index] = false
        }
        
        selectedStats[indexPath.row] = true
        cvBodyFat.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size = CGSize(width: (collectionViewWidth-20)/3.0, height: (collectionViewWidth-20)/3.0)
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
