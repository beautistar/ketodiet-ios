//
//  DailyIntakeViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class DailyIntakeViewController: BaseViewController {

    @IBOutlet weak var intakeView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var carbTitleView: UIView!
    @IBOutlet weak var carbView: UIView!
    @IBOutlet weak var fatTitleView: UIView!
    @IBOutlet weak var fatView: UIView!
    @IBOutlet weak var caloriesView: UIView!
    @IBOutlet weak var caloriesTitleView: UIView!
    @IBOutlet weak var proteinTitleView: UIView!
    @IBOutlet weak var proteinView: UIView!
    
    @IBOutlet weak var lblNetCarbs: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
        setValues()
    }
    
    func initView() {
        
        nextView.layer.borderColor = UIColor.white.cgColor
        
        let views = [carbTitleView, carbView, fatTitleView, fatView, caloriesTitleView, caloriesView, proteinTitleView, proteinView]
        
        for view in views {
            view?.layer.borderColor = UIColor.white.cgColor
            view?.layer.borderWidth = 2.0
            view?.clipsToBounds = true
            view?.layer.cornerRadius = 10
            view?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }        
    }
    
    func setValues() {
        
        let nustritionHelper = NutritionGoalsHelper(user: user)
        
        lblNetCarbs.text = String(format: "%.1f", nustritionHelper.getNetCarbs())
        lblFat.text = String(format: "%.1f", nustritionHelper.getFat())
        lblCalories.text = String(format: "%.1f", nustritionHelper.getCalories())
        lblProtein.text = String(format: "%.1f", nustritionHelper.getProtein())
    }
    
    @IBAction func continueAction(_ sender: Any) {
        
        user.isStarted = true
        currentUser = user
        
        self.dismiss(animated: true, completion: nil)
        
        createMenuView()
    }
    
    fileprivate func createMenuView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController        
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = Const.colorTop
        nvc.navigationBar.barTintColor = UIColor.white
        
        menuViewController.homeViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }
}
