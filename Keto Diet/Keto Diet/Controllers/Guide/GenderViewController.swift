//
//  GenderViewController.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class GenderViewController: BaseViewController {

    @IBOutlet weak var maleView: UIView!
    @IBOutlet weak var femaleView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if getCurrentUser().isStarted == true {
            createMenuView()
        }
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        initView()
    }
    
    func initView() {
        
        maleView.layer.borderColor = Const.colorTop.cgColor
        femaleView.layer.borderColor = UIColor.white.cgColor
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "male" {
            user.gender = Const.MALE
        } else {
            user.gender = Const.FEMALE
        }
    }
    
    fileprivate func createMenuView() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let mainViewController = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        let menuViewController = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        let nvc: UINavigationController = UINavigationController(rootViewController: mainViewController)
        
        UINavigationBar.appearance().tintColor = Const.colorTop
        nvc.navigationBar.barTintColor = UIColor.white
        
        menuViewController.homeViewController = nvc
        
        let slideMenuController = ExSlideMenuController(mainViewController:nvc, leftMenuViewController: menuViewController)
        slideMenuController.delegate = mainViewController
        UIApplication.shared.keyWindow?.rootViewController = slideMenuController
        UIApplication.shared.keyWindow?.makeKeyAndVisible()
        
    }

}
