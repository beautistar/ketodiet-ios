//
//  ActivityViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class ActivityViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var tblLevel: UITableView!
    
    var isSelect: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func initView() {
        
        nextView.layer.borderColor = UIColor.white.cgColor
        activityView.layer.borderColor = UIColor.white.cgColor
        activityView.layer.borderWidth = 2.0
        activityView.clipsToBounds = true
        activityView.layer.cornerRadius = 10
        activityView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        user.activityLevel = 0
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if !isSelect {
             self.showAlertDialog(title: Const.AppName, message: "Please select your activity level", positive: "OK", negative: nil)
        
        } else {
            let objectiveVC = self.storyboard?.instantiateViewController(withIdentifier: "ObjectiveViewController") as! ObjectiveViewController
            self.navigationController?.pushViewController(objectiveVC, animated: true)
        }
       
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     
        let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityLevelCell") as! ActivityLevelCell
        
        cell.imvActivity.image = Const.activityLevelImages[indexPath.row]
        cell.lblTitle.text = Const.activityLevelTitles[indexPath.row]
        cell.lblContent.text = Const.activityLevelContents[indexPath.row]
        
       
        return cell
     
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isSelect = true
        user.activityLevel = indexPath.row
    }

}
