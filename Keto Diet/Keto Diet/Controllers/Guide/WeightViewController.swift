//
//  WeightViewController.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class WeightViewController: BaseViewController {

    @IBOutlet weak var weightView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var tfWeight: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let numberToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
                               UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneAction(_ :)))]
        tfWeight.inputAccessoryView = numberToolbar
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initView()
    }

    
    func initView() {
        
        nextView.layer.borderColor = UIColor.white.cgColor
        weightView.layer.borderColor = UIColor.white.cgColor
        weightView.layer.borderWidth = 2.0
        weightView.clipsToBounds = true
        weightView.layer.cornerRadius = 10
        weightView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
        
        lblUnit.text = "kgs"
        user.weightType = Const.TYPE_WEIGHT_KG
        
    }
    
    @objc func doneAction(_ sender: Any) {
        
        tfWeight.resignFirstResponder()
        
    }
    
    @IBAction func unitAction(_ sender: Any) {
        
        if lblUnit.text == "lbs" {
            lblUnit.text = "kgs"
            user.weightType = Const.TYPE_WEIGHT_KG
        } else {
            lblUnit.text = "lbs"
            user.weightType = Const.TYPE_WEIGHT_LBS
        }        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        
        if tfWeight.text?.count == 0 {
            self.showAlertDialog(title: Const.AppName, message: "Please enter your weight", positive: "OK", negative: nil)
        } else {
            user.weight = Float(tfWeight.text!)!
            
            gotoNext()
        }
    }
    
    func gotoNext() {
        
        let bodyFatVC = self.storyboard?.instantiateViewController(withIdentifier: "BodyFatViewController") as! BodyFatViewController
        self.navigationController?.pushViewController(bodyFatVC, animated: true)
        
    }
}
