//
//  PurchaseViewController.swift
//  Keto Diet
//
//  Created by Developer on 11/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class PurchaseViewController: BaseViewController {
    
    @IBOutlet weak var reCalcView: UIView!
    @IBOutlet weak var changeView: UIView!
    @IBOutlet weak var restoreView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: Const.AppName, message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("purchase succeed")
                    
                    let userdefault = UserDefaults.standard
                    userdefault.set(true, forKey: Const.KEY_ISPURCHASED)
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            
            } else if type == .restored {
                
                self?.hideLoadingView()
                let alertView = UIAlertController(title: Const.AppName, message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("restore succeed")
                    
                    let userdefault = UserDefaults.standard
                    userdefault.set(true, forKey: Const.KEY_ISPURCHASED)
                    self?.navigationController?.popViewController(animated: true)
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)                
            } else {
                self?.hideLoadingView()
                self?.showToast("failed")
            }
        }
        
        initView()
    }
    
    func initView() {
        
        reCalcView.layer.borderColor = UIColor.white.cgColor
        changeView.layer.borderColor = Const.colorTop.cgColor
        restoreView.layer.borderColor = UIColor.white.cgColor
        
        // shadow effect
        reCalcView.layer.shadowColor = UIColor.black.cgColor
        reCalcView.layer.shadowOpacity = 0.5
        reCalcView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        reCalcView.layer.shadowRadius = 5
        
        // shadow effect
        changeView.layer.shadowColor = UIColor.black.cgColor
        changeView.layer.shadowOpacity = 0.5
        changeView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        changeView.layer.shadowRadius = 5
        
        restoreView.layer.shadowColor = UIColor.black.cgColor
        restoreView.layer.shadowOpacity = 0.5
        restoreView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        restoreView.layer.shadowRadius = 5
    }
    
    @IBAction func purchaseAction(_ sender: Any) {
        
        IAPHandler.shared.purchaseMyProduct(index: 0)
    }
    
    @IBAction func laterAction(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func restoreAction(_ sender: Any) {
        
        self.showLoadingView()
        IAPHandler.shared.restorePurchase()
    }
    

}
