//
//  SettingViewController.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class SettingViewController: BaseViewController {

    @IBOutlet weak var reCalcView: UIView!
    @IBOutlet weak var changeView: UIView!
    @IBOutlet weak var removeAdsView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        IAPHandler.shared.fetchAvailableProducts()
        IAPHandler.shared.purchaseStatusBlock = {[weak self] (type) in
            guard let strongSelf = self else{ return }
            if type == .purchased {
                let alertView = UIAlertController(title: Const.AppName, message: type.message(), preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    //TODO: success purchased
                    print("purchase succeed")
                    
                    let userdefault = UserDefaults.standard
                    userdefault.set(true, forKey: Const.KEY_ISPURCHASED)
                    
                })
                alertView.addAction(action)
                strongSelf.present(alertView, animated: true, completion: nil)
            }
        }
        
        setNavigationBarItem()
        
        initView()
    }
    
    func initView() {
        
        self.title = "Setting"
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.colorTop
        
        reCalcView.layer.borderColor = UIColor.white.cgColor
        changeView.layer.borderColor = Const.colorTop.cgColor
        removeAdsView.layer.borderColor = UIColor.white.cgColor
        
        // shadow effect
        reCalcView.layer.shadowColor = UIColor.black.cgColor
        reCalcView.layer.shadowOpacity = 0.5
        reCalcView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        reCalcView.layer.shadowRadius = 5
        
        // shadow effect
        changeView.layer.shadowColor = UIColor.black.cgColor
        changeView.layer.shadowOpacity = 0.5
        changeView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        changeView.layer.shadowRadius = 5
        
        removeAdsView.layer.shadowColor = UIColor.black.cgColor
        removeAdsView.layer.shadowOpacity = 0.5
        removeAdsView.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        removeAdsView.layer.shadowRadius = 5
    }
    
    
    @IBAction func resetAction(_ sender: Any) {
        
        self.showAlert(title: Const.AppName, message: Const.MSG_RESET_GOAL, okButtonTitle: "Yes", cancelButtonTitle: "No") {
            
            let user = UserModel()
            let nutrition = NutritionModel()
            
            currentUser = user
            currentNutrition = nutrition
            resetHistory()
            
            let initNav = self.storyboard?.instantiateViewController(withIdentifier: "InitNav") as! UINavigationController
            
            UIApplication.shared.keyWindow?.rootViewController = initNav
        }
    }
    
    
    @IBAction func editAction(_ sender: Any) {
        
        let user = UserModel()
        currentUser = user
        
        let initNav = self.storyboard?.instantiateViewController(withIdentifier: "InitNav") as! UINavigationController
        
        UIApplication.shared.keyWindow?.rootViewController = initNav
        
    }
    
    @IBAction func removeAds(_ sender: Any) {
        
        
    }
    
}
