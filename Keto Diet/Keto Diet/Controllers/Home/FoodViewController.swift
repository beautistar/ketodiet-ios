//
//  FoodViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Presentr

class FoodViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var tfFood: UITextField!
    @IBOutlet weak var cvAutoComplete: UICollectionView!
    @IBOutlet weak var tvFoodList: UITableView!
    
    var autoCompleteStrings = [String]()
    var foodList = [FoodModel]()
    var selectedFood = FoodModel()
    var selectedFoodId: String = ""
    var selectedServing = ServingModel()
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    

    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        boxView.layer.borderWidth = 2.0
        boxView.layer.borderColor = UIColor.white.cgColor
        boxView.clipsToBounds = true
        boxView.layer.cornerRadius = 10
        boxView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        cvAutoComplete.isHidden = true
        
        tfFood.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func getFoodAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        selectedFoodId = foodList[btn.tag].id
        
        getFood()
    }
    
    func getFood() {
        
        self.showLoadingView()
        
        ApiRequest.getFood(selectedFoodId, from: Const.FROM_FOODLIST) { (resCode, food) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                self.selectedFood = food
                self.presentAddVC()
            }
        }
    }

    @IBAction func barcodeAction(_ sender: Any) {
        
        let barcodeVC = self.storyboard?.instantiateViewController(withIdentifier: "BarcodeViewController") as! BarcodeViewController
        barcodeVC.foodVC = self
        barcodeVC.from = Const.FROM_FOODLIST
        self.present(barcodeVC, animated: true)
    }
    
    //MARK: - Barcode ViewController dismiss event
    func dismissProcess(_ food: FoodModel) {
        
        foodList.removeAll()
        selectedFood = food
        
        presentAddVC()
    }
    
    func presentAddVC() {
        
        let addConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmViewController") as! AddConfirmViewController
        addConfirmVC.selectedFood = self.selectedFood
        addConfirmVC.addNutritionVC = self.parent?.parent?.parent as? AddNutritionViewController
        addConfirmVC.homeVC = self.parent?.parent?.parent?.parent as? HomeViewController
        addConfirmVC.from = Const.FROM_FOODLIST
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 450), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: addConfirmVC, animated: true, completion: nil)
    }
    

    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return foodList.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell") as! FoodCell

        cell.addButton.tag = indexPath.row
        cell.addButton.addTarget(self, action: #selector(getFoodAction(_:)), for: .touchUpInside)
        cell.lblTitle.text = foodList[indexPath.row].name
        
        return cell
    }
    
    
    //MARK: - CollectionView
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return autoCompleteStrings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AutoCompeteCell", for: indexPath) as! AutoCompeteCell
        cell.lblFoodName.text = autoCompleteStrings[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        cvAutoComplete.isHidden = true
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.showTopView()
        self.view.endEditing(true)
        
        foodList.removeAll()
        self.tvFoodList.reloadData()
        tfFood.text = autoCompleteStrings[indexPath.row]
        
        ApiRequest.foodSearch(tfFood.text!) { (resCode, foods) in
            
            if resCode == Const.CODE_SUCCESS {
                
                self.foodList = foods
                self.tvFoodList.reloadData()
                
            } else {
                
                self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.view.frame.width - 20, height: 30)
    }
    
    //MARK: - TextField
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.hideTopView()
        
        cvAutoComplete.isHidden = false
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {        
        
        cvAutoComplete.isHidden = false
        
        let foodName = textField.text!
        
        if foodName.count > 0 {            
            
            foodList.removeAll()
            tvFoodList.reloadData()
            
            autoCompleteStrings.removeAll()
            
            ApiRequest.autoComplete(foodName) { (resCode, strings) in
                
                if resCode == Const.CODE_SUCCESS {
                    
                    self.autoCompleteStrings = strings
                    self.cvAutoComplete.reloadData()
                
                } else {
                
                    self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
                
                }
            }
        
        } else {
        
            cvAutoComplete.isHidden = true
        }
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.view.endEditing(true)
        
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.showTopView()
        
        return true
    }
}
