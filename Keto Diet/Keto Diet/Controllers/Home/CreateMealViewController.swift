//
//  CreateMealViewController.swift
//  Keto Diet
//
//  Created by Developer on 11/7/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Presentr

class CreateMealViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    

    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var tfMealName: UITextField!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var caloriesView: UIView!
    @IBOutlet weak var fatView: UIView!
    @IBOutlet weak var proteinView: UIView!
    @IBOutlet weak var carbView: UIView!
    
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    
    @IBOutlet weak var tfCalories: UITextField!
    @IBOutlet weak var tfFat: UITextField!
    @IBOutlet weak var tfCarbs: UITextField!
    @IBOutlet weak var tfProtein: UITextField!
    @IBOutlet weak var customFoodAddView: UIView!
    @IBOutlet weak var tblFoodList: UITableView!
    
    @IBOutlet weak var tfFood: UITextField!
    var isCustomStatus: Bool = false
    var isAutoFillStatus: Bool = true
    
    var foodList = [HistoryModel]()
    var autoCompleteStrings = [String]()
    var selectedFoodID = ""
    var selectedFood = FoodModel()
    var selectedIndex = 0
    var selectedFoodList = [HistoryModel]()
    var flags = [Bool]()
    var mealList = [MealModel]()
    var byScanning: Bool = false
    
    var inputfiels = [UITextField]()
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    //MARK: - life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let views: [UIView] = [nameView, boxView, caloriesView, fatView, proteinView, carbView]
        inputfiels = [tfCalories, tfFat, tfCarbs, tfProtein]
        
        for view in views {
            
            view.layer.borderWidth = 2.0
            view.layer.borderColor = UIColor.white.cgColor
            view.clipsToBounds = true
            view.layer.cornerRadius = 10
            view.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
            
        }
        
        tblFoodList.tableFooterView = UIView()
        
        customFoodAddView.isHidden = true
        
        tfFood.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        
        // load saved my meal list
        if let data = UserDefaults.standard.data(forKey: Const.KEY_MYMEAL) ,
            let mealList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [MealModel] {
            self.mealList = mealList
        }
    }
    
    @IBAction func addSelectedFoodAction(_ sender: Any) {
        
        if tfMealName.text?.count == 0 {
            
            self.showAlertDialog(title: Const.AppName, message: "Please input mean name", positive: "OK", negative: nil)
            
            return
        }
        
        if selectedFoodList.count == 0 {
            
            self.showAlertDialog(title: Const.AppName
                , message: "Please select food(s)", positive: "OK", negative: nil)
            
            return
        }
        
        let meal = MealModel.init(name: tfMealName.text!, nutrition: selectedFoodList)
        mealList.append(meal)
        
        
        //  Save data to history and recent
        let userDefaults = UserDefaults.standard        
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: mealList)
        userDefaults.set(encodedData, forKey: Const.KEY_MYMEAL)
        userDefaults.synchronize()
        
        self.showToast("Meal is added successfully")
        
        initData()
    }
    
    
    func initData() {
        
        //  initialize all data
        for inputfield in inputfiels {
            inputfield.text = ""
        }
        byScanning = false
        lblCarbs.text = ""
        lblCalories.text = ""
        lblFat.text = ""
        lblProtein.text = ""
        tfMealName.text = ""
        tfFood.text = ""
        autoCompleteStrings.removeAll()
        selectedFoodList.removeAll()
        flags.removeAll()
        isAutoFillStatus = true
        tblFoodList.reloadData()
        
        
        
    }
    
    
    @IBAction func customFoodAction(_ sender: Any) {
        
        isCustomStatus = !isCustomStatus
        
        if isCustomStatus {
            customFoodAddView.isHidden = false
            tfFood.isUserInteractionEnabled = false
        } else {
            customFoodAddView.isHidden = true
            tfFood.isUserInteractionEnabled = true
        }
    }
    
    @IBAction func addCustomFoodAction(_ sender: Any) {
        
        if tfMealName.text?.count == 0 {
            
            self.showAlertDialog(title: Const.AppName, message: "Please input meal name", positive: "OK", negative: nil)
            
            return
        }
        
        for inputfield in inputfiels {
            
            if inputfield.text?.count == 0 {
                
                self.showAlertDialog(title: Const.AppName, message: "Input food macros", positive: "OK", negative: nil)
                
                return
            }
            
        }
        
        lblCalories.text = tfCalories.text
        lblCarbs.text = tfCarbs.text
        lblFat.text = tfFat.text
        lblProtein.text = tfProtein.text
        
        let foodHistoryModel = HistoryModel(foodId: tfMealName.text!, foodName: tfMealName.text!, carbs: Float(tfCarbs.text!)!, calories: Int(tfCalories.text!)!, fat: Float(tfFat.text!)!, protein: Float(tfProtein.text!)!, addDate: Date())
        
        selectedFoodList.append(foodHistoryModel)
        
        let meal = MealModel(name: tfMealName.text!, nutrition: selectedFoodList)
        mealList.append(meal)
        
        
        //  Save data to history and recent
        saveMyMeals(mealList)
        
        self.showToast("Meal is added successfully")
        
        initData()
    }
    
    
    @IBAction func scanFoodAction(_ sender: Any) {
        
        let barcodeVC = self.storyboard?.instantiateViewController(withIdentifier: "BarcodeViewController") as! BarcodeViewController
        barcodeVC.mealVC = self
        barcodeVC.from = Const.FROM_CREATEMEAL
        self.present(barcodeVC, animated: true)
        
    
    }    
    
    //MARK: - Barcode ViewController dismiss event
    func dismissProcess(_ food: FoodModel) {
        
        byScanning = true
        selectedFood = food
        
        presentAddVC()
    }
    
    
    func getFood() {
        
        self.showLoadingView()
        
        ApiRequest.getFood(selectedFoodID, from: Const.FROM_FOODLIST) { (resCode, food) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                self.selectedFood = food
                self.presentAddVC()
            }
        }
    }

    
    func presentAddVC() {
        
        let addConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmViewController") as! AddConfirmViewController
        addConfirmVC.selectedFood = self.selectedFood
        addConfirmVC.createMealVC = self
        
        addConfirmVC.from = Const.FROM_CREATEMEAL
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 450), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: addConfirmVC, animated: true, completion: nil)
    }
    
    func dismissServing(_ serving: ServingModel) {
        
        if byScanning {
            
            let foodHistoryModel = HistoryModel(foodId: selectedFood.id, foodName: selectedFood
                .name, carbs: ENABLE_NETCARB ? serving.carbohydrate - serving.fiber : serving.carbohydrate , calories: serving.calories, fat: serving.fat, protein: serving.protein, addDate: Date())
            
            selectedFoodList.append(foodHistoryModel)
            
            lblCarbs.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.carbs} ).reduce(0, +))
            lblCalories.text = String(format: "%.1fkcal", Float(self.selectedFoodList.map( {$0.calories} ).reduce(0, +)))
            lblFat.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.fat} ).reduce(0, +))
            lblProtein.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.protein} ).reduce(0, +))
            
            byScanning = false
            
        } else {
            
            let food = foodList[selectedIndex]
            
            flags[selectedIndex] = !flags[selectedIndex]
            
            if flags[selectedIndex] {
                
                selectedFoodList.append(food)
                
            } else {
                
                selectedFoodList = selectedFoodList.filter( {$0.foodId != foodList[selectedIndex].foodId})
                
            }
            
            
            foodList[selectedIndex].calories = serving.calories
            foodList[selectedIndex].carbs = (ENABLE_NETCARB ? serving.carbohydrate - serving.fiber : serving.carbohydrate)
            foodList[selectedIndex].protein = serving.protein
            foodList[selectedIndex].fat = serving.fat
            foodList[selectedIndex].addDate = Date()
            
            tblFoodList.reloadData()
            
            lblCarbs.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.carbs} ).reduce(0, +))
            lblCalories.text = String(format: "%.1fkcal", Float(self.selectedFoodList.map( {$0.calories} ).reduce(0, +)))
            lblFat.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.fat} ).reduce(0, +))
            lblProtein.text = String(format: "%.1fg", self.selectedFoodList.map( {$0.protein} ).reduce(0, +))
            
        }
        
    }
    
    
    //MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if isAutoFillStatus {
            
            return autoCompleteStrings.count
        } else {
            return foodList.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if isAutoFillStatus {
            return 40
        } else {
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isAutoFillStatus {
            
            let cell = UITableViewCell(style: .default, reuseIdentifier: "FoodListCell")
            cell.textLabel?.textColor = UIColor.gray
            cell.textLabel?.text = autoCompleteStrings[indexPath.row]
            cell.backgroundColor = UIColor.clear
            
            tableView.separatorStyle = .singleLine
            
            return cell
            
        }
            
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell") as! FoodCell
      
            if flags[indexPath.row] {
                cell.imvCheck.image = UIImage(named: "ic_checked")
            } else {
                cell.imvCheck.image = UIImage(named: "ic_unchecked")
            }
            let selectedFood = foodList[indexPath.row]
            cell.lblTitle.text = selectedFood.foodName
            
            if selectedFood.carbs == 0.0 && selectedFood.calories == 0 && selectedFood.protein == 0.0 && selectedFood.fat == 0.0 {
                
                cell.lblContents.isHidden = true
            } else {
                cell.lblContents.isHidden = false
                
            }
            cell.lblContents.text = String(format: "%.1fg Carbs - %.1fkcal - %.1fg Fat - %.1fg Protein", selectedFood.carbs, Float(selectedFood.calories), selectedFood.fat, selectedFood.protein)
            tableView.separatorStyle = .none
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isAutoFillStatus {
            
            isAutoFillStatus = false
            
            self.view.endEditing(true)
            
            foodList.removeAll()
            flags.removeAll()
            
            self.tblFoodList.reloadData()
            
            tfFood.text = autoCompleteStrings[indexPath.row]
            
            ApiRequest.foodSearch(tfFood.text!) { (resCode, foods) in
                
                if resCode == Const.CODE_SUCCESS {
                    
                    for food in foods {
                        let foodHistoryModel = HistoryModel(foodId: food.id, foodName: food.name, carbs: 0, calories: 0, fat: 0, protein: 0, addDate: Date())
                        self.foodList.append(foodHistoryModel)
                        self.flags.append(false)
                    }
                    self.tblFoodList.reloadData()
                    
                } else {
                    
                    self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
                }
            }
            
        } else {
            
            selectedIndex = indexPath.row
            selectedFoodID = foodList[indexPath.row].foodId
            getFood()
            
        }
        
    }
    
    //MARK: - TextField
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == tfMealName { return true }        
        
        isAutoFillStatus = true
        
        foodList.removeAll()
        flags.removeAll()
        autoCompleteStrings.removeAll()
        
        tblFoodList.reloadData()
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let foodName = textField.text!
        
        if foodName.count > 0 {
            
            foodList.removeAll()
            flags.removeAll()
            autoCompleteStrings.removeAll()
            
            tblFoodList.reloadData()

            
            ApiRequest.autoComplete(foodName) { (resCode, strings) in
                
                if resCode == Const.CODE_SUCCESS {
                    
                    self.autoCompleteStrings = strings
                    self.tblFoodList.reloadData()
                    
                } else {
                    
                    self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
                    
                }
            }
        }
    }
    
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        self.view.endEditing(true)
        
        return true
    }
    
}
