//
//  CalendarViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/20/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import VACalendar

class CalendarViewController: BaseViewController {

    @IBOutlet weak var lblCopyFood: UILabel!
    @IBOutlet weak var lblWeek: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "LLLL yyyy"
            
            let appereance = VAMonthHeaderViewAppearance(
                previousButtonImage: UIImage(),
                nextButtonImage: UIImage(),
                dateFormatter: dateFormatter
            )
            
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
        }
        
    }
    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .short, calendar: defaultCalendar)
            
            weekDaysView.appearance = appereance
            weekDaysView.tintColor = UIColor.gray
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    var calendarView: VACalendarView!

    var homeVC: HomeViewController?
    var from: Int = 0
    
    //MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if from == Const.FROM_COPY {
            lblCopyFood.isHidden = false
        } else {
            lblCopyFood.isHidden = true
        }
        
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        calendarView.showDaysOut = true
        calendarView.selectionStyle = .single
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        
        self.contentView.addSubview(calendarView)        
        
        setDate(date: SelectedDay)
        
        SelectedDay = Date()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        
        
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 0,
                y: 0,
                width: weekDaysView.frame.width * 0.9,
                height: contentView.frame.height
            )
            calendarView.setup()
            
        }
    }
    
    func setDate(date: Date) {
        
        
        let calendar = NSCalendar.current
        
        print(date)
        let year =  calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        let week = calendar.component(.weekday, from: date)
        
        print(week)
        lblYear.text = "\(year)"
        lblMonth.text = "\(calendar.monthSymbols[month-1])".uppercased()
        lblWeek.text = "\(calendar.weekdaySymbols[week-1])".uppercased()
        lblDay.text = "\(day)"
        
        
        
    }
    
    @IBAction func okAction(_ sender: Any) {
            
        if self.from == Const.FROM_DATE { // change dashboard
            
            self.dismiss(animated: true) {
            
                self.homeVC?.loadFoodList(SelectedDay, from: self.from)
            }
            
        } else { // copy today data
            
            self.showAlert(title: Const.AppName, message: Const.MSG_COPY_CONFIRM, okButtonTitle: "Yes", cancelButtonTitle: "No", okClosure: {
                
                self.dismiss(animated: true) {
                
                    self.homeVC?.loadFoodList(SelectedDay, from: self.from)
                }
                
            })
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CalendarViewController: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension CalendarViewController: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 15, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .gray
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return Const.colorTop
    }
}

extension CalendarViewController: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return .clear
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return .gray
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return Const.colorTop
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension CalendarViewController: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        
        calendarView.startDate = dates.last ?? Date()

        SelectedDay = dates.last ?? Date()
        
        setDate(date: SelectedDay)

    }
    
}
