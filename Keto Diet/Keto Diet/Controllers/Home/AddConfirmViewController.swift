//
//  AddConfirmViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/16/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class AddConfirmViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblButtonName: UILabel!
    
    @IBOutlet weak var intakeView: UIView!
    @IBOutlet weak var nextView: UIView!
    @IBOutlet weak var carbTitleView: UIView!
    @IBOutlet weak var fatTitleView: UIView!
    @IBOutlet weak var fatView: UIView!
    @IBOutlet weak var caloriesView: UIView!
    @IBOutlet weak var caloriesTitleView: UIView!
    @IBOutlet weak var proteinTitleView: UIView!
    @IBOutlet weak var proteinView: UIView!
    
    @IBOutlet weak var lblServingTitle: UILabel!
    @IBOutlet weak var lblFoodName: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    @IBOutlet weak var lblFiber: UILabel!
    @IBOutlet weak var tblServingList: UITableView!
    @IBOutlet weak var tblServingsListHeightConstraint: NSLayoutConstraint!
    
    var servingListCellHeight = 40
    var selectedFood = FoodModel()
    var selectedServing = ServingModel()
    var selectedHistory: HistoryModel?
    var addNutritionVC: AddNutritionViewController?
    var homeVC: HomeViewController?
    var createMealVC: CreateMealViewController?
    var from: Int = 0
    
    
    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        initView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if tblServingsListHeightConstraint.constant > CGFloat(selectedFood.servings.count * servingListCellHeight) {
            
            tblServingsListHeightConstraint.constant = CGFloat(selectedFood.servings.count * servingListCellHeight)
        }
    }

    func initView() {
        
        if from == Const.FROM_HOME {
            lblTitle.text = "Edit Food"
            lblButtonName.text = "Edit Food"
            
        } else if from == Const.FROM_CREATEMEAL {
            
            lblTitle.text = "Select Serving"
            lblButtonName.text = "Select Serving"
            
        } else {
            lblTitle.text = "Add Nutrition"
            lblButtonName.text = "Add Nutrition"
        }
        
        tblServingList.tableFooterView = UIView()      

        selectedServing = selectedFood.servings[0]
        
        tblServingList.isHidden = true
        
        nextView.layer.borderColor = UIColor.white.cgColor
        
        let views = [carbTitleView, fatTitleView, fatView, caloriesTitleView, caloriesView, proteinTitleView, proteinView]
        
        for view in views {
            view?.layer.borderColor = UIColor.white.cgColor
            view?.layer.borderWidth = 2.0
            view?.clipsToBounds = true
            view?.layer.cornerRadius = 10
            view?.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        }
        
        setValues()
    }
    
    func setValues() {
        
        lblServingTitle.text = selectedServing.serving_description
        lblFoodName.text = selectedFood.name
        lblCalories.text = String(format: "Calories : %d", selectedServing.calories)
        lblFat.text = String(format: "Fat : %.1f", selectedServing.fat)
        lblCarbs.text = String(format: "Carbs : %.1f", ENABLE_NETCARB ? self.selectedServing.carbohydrate - self.selectedServing.fiber : self.selectedServing.carbohydrate)
        lblProtein.text = String(format: "Protein : %.1f", selectedServing.protein)
        lblFiber.text = String(format: "Fiber : %.1f", selectedServing.fiber)
    }
    
    @IBAction func addAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            
            let nutrition = NutritionModel()
            
            if self.from == Const.FROM_HOME { // food edit case
                
                nutrition.calories = getcurrentNutrition().calories - Float(self.selectedHistory!.calories) + Float(self.selectedServing.calories)
                nutrition.carbs = getcurrentNutrition().carbs - self.selectedHistory!.carbs + (ENABLE_NETCARB ? self.selectedServing.carbohydrate - self.selectedServing.fiber : self.selectedServing.carbohydrate)
                nutrition.fat = getcurrentNutrition().fat - self.selectedHistory!.fat + self.selectedServing.fat
                nutrition.protein = getcurrentNutrition().protein - self.selectedHistory!.protein + self.selectedServing.protein
                nutrition.fiber = self.selectedServing.fiber
            
            } else if self.from == Const.FROM_CREATEMEAL {
                
                self.createMealVC?.dismissServing(self.selectedServing)
                
                return
                
            } else { // food add case
                
                nutrition.calories = getcurrentNutrition().calories + Float(self.selectedServing.calories)
                nutrition.carbs = getcurrentNutrition().carbs + (ENABLE_NETCARB ? self.selectedServing.carbohydrate - self.selectedServing.fiber : self.selectedServing.carbohydrate)
                nutrition.fat = getcurrentNutrition().fat + self.selectedServing.fat
                nutrition.protein = getcurrentNutrition().protein + self.selectedServing.protein
                nutrition.fiber = self.selectedServing.fiber
                
                
            }

            
            currentNutrition = nutrition
           
            self.addNutritionVC?.setValues()            
            
            // Save history
            
            let calendar = Calendar.current
            let dateComponent = calendar.dateComponents([.year, .month, .day], from: SelectedDay)
            let curDate = calendar.date(from: dateComponent)
            
            
            let historyModel = HistoryModel(foodId: self.selectedFood.id, foodName: self.selectedFood.name, carbs: self.selectedServing.carbohydrate, calories: self.selectedServing.calories, fat: self.selectedServing.fat, protein: self.selectedServing.protein, addDate: curDate!)
            
            
            if self.from == Const.FROM_HOME {
                
                for index in 0...HistoryList.count - 1 {
                    if HistoryList[index].foodId == historyModel.foodId {
                        HistoryList.remove(at: index)
                        HistoryList.insert(historyModel, at: index)
                        break
                    }
                }
                
            } else {
                HistoryList.append(historyModel)
                
                // add to recent if food is different
                if !RecentHistoryList.contains(where: { $0.foodId == historyModel.foodId } ) {
                    RecentHistoryList.append(historyModel)
                }
                
                if RecentHistoryList.count > 5 { // delete if history is more than 5
                    RecentHistoryList.removeFirst()
                }
            }
            
            // Save data to history and recent
            
            saveHistory(HistoryList)
            saveRecent(RecentHistoryList)            

            
            self.homeVC?.setValues()
            self.homeVC?.loadFoodList(SelectedDay, from: Const.FROM_DATE)
        })
    }
    
    @IBAction func downAction(_ sender: Any) {
    
        tblServingList.isHidden = !tblServingList.isHidden
        
    }
    
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return selectedFood.servings.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(servingListCellHeight)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell.init(style: .default, reuseIdentifier: "ServingCell")
        cell.textLabel?.text = selectedFood.servings[indexPath.row].serving_description
        cell.textLabel?.textColor = UIColor.lightGray
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tblServingList.isHidden = true
        
        selectedServing = selectedFood.servings[indexPath.row]
        setValues()
        
        self.showToast("Servings: " + selectedServing.serving_description)
    }
}
