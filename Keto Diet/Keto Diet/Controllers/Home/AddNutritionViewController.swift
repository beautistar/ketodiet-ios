//
//  AddNutritionViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import CarbonKit
import GoogleMobileAds

class AddNutritionViewController: BaseViewController, CarbonTabSwipeNavigationDelegate {

    @IBOutlet weak var lblToday: UILabel!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imvBg: UIImageView!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    
    @IBOutlet weak var progCalories: UIProgressView!
    @IBOutlet weak var progCarbs: UIProgressView!
    @IBOutlet weak var progProtein: UIProgressView!
    @IBOutlet weak var progFat: UIProgressView!
    
    @IBOutlet weak var divideLine: UILabel!
    @IBOutlet weak var contentViewYConstraint: NSLayoutConstraint!
    @IBOutlet weak var topViewHeightContraint: NSLayoutConstraint!
    
    @IBOutlet weak var bannerView: GADBannerView!

    
    var carbonTabSwipeNavigation: CarbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
            
            bannerView.isHidden = true
            
        } else {
            
            bannerView.adUnitID = Const.bannerID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
            
        }
        /*
        imvBg.clipsToBounds = true
        imvBg.layer.cornerRadius = 10
        imvBg.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        */
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: Const.nutritionItems, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: contentView)
        
        self.style()
        
        self.setValues()
    }
    
    
    
    func style() {
        
        let tabWidth = ((self.view.bounds.width - 20) / 4.0)
        
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.colorTop
    
        let toolbar = carbonTabSwipeNavigation.toolbar
        toolbar.isTranslucent = true
        toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .any, barMetrics: .default)
        
        carbonTabSwipeNavigation.setIndicatorColor(UIColor.white)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        if let segControl = carbonTabSwipeNavigation.carbonSegmentedControl {
            for i in 0...3 {
                segControl.setWidth(tabWidth, forSegmentAt: i)
            }
        }
    carbonTabSwipeNavigation.setNormalColor(UIColor.white.withAlphaComponent(0.6))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont.boldSystemFont(ofSize: 14))
        
    }
    
    func setValues() {
        
        let calendar = Calendar.current
        let dateComponent = calendar.dateComponents([.year, .month, .day], from: SelectedDay)
        let selDate = calendar.date(from: dateComponent)
        
        let todayComponent = calendar.dateComponents([.year, .month, .day], from: Date())
        let todayDate = calendar.date(from: todayComponent)
        
        // todayDate    Date?    2018-11-02 04:00:00 UTC    some
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if selDate == todayDate {
            lblToday.text = "Today"
            
        } else {
            lblToday.text = formatter.string(from: selDate!)
        }
        
        let savedNutrition = getcurrentNutrition()
        let nutrition = NutritionGoalsHelper(user: getCurrentUser())
        
        lblCalories.text = String(format: "%.1fkcal / %.1fkcal", savedNutrition.calories, nutrition.getCalories())
        lblCarbs.text = String(format: "%.1fg / %.1fg", round(10 * savedNutrition.carbs)/10, nutrition.getNetCarbs())
        lblProtein.text = String(format: "%.1fg / %.1fg", savedNutrition.protein, nutrition.getProtein())
        lblFat.text = String(format: "%.1fg / %.1fg", savedNutrition.fat, nutrition.getFat()) 
        
        UIView.animate(withDuration: 2.0) {
            self.progCalories.setProgress(Float(savedNutrition.calories) / nutrition.getCalories(), animated: true)
            self.progProtein.setProgress(savedNutrition.protein / nutrition.getProtein(), animated: true)
            self.progFat.setProgress(savedNutrition.fat / nutrition.getFat(), animated: true)
            self.progCarbs.setProgress(savedNutrition.carbs /  nutrition.getNetCarbs(), animated: true)
        }

    }

    
    func hideTopView() {
        
        UIView.animate(withDuration: 0.3) {
            
            self.divideLine.isHidden = true
            self.lblToday.isHidden = true
            self.contentViewYConstraint.constant = 0
            self.topViewHeightContraint.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func showTopView() {
        
        UIView.animate(withDuration: 0.3) {
            
            self.divideLine.isHidden = false
            self.lblToday.isHidden = false
            self.contentViewYConstraint.constant = 200
            self.topViewHeightContraint.constant = 140
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction override func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true )
    }

    
    // MARK: - CarbonKit Delegate

    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        
        switch index {
        case 0:
            let foodVC = self.storyboard!.instantiateViewController(withIdentifier: "FoodViewController") as! FoodViewController
            return foodVC
        case 1:
            return self.storyboard!.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController
        case 2:
            return self.storyboard!.instantiateViewController(withIdentifier: "MealViewController") as! MealViewController
        default:
            return self.storyboard!.instantiateViewController(withIdentifier: "RecentViewController") as! RecentViewController
        }
        
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt) {
        NSLog("Did move at index: %ld", index)
    }

}
