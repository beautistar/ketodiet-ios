//
//  MealViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class MealViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tbNutritionList: UITableView!
    @IBOutlet weak var boxView: UIView!
    
    var myMealList = [MealModel]()
    var checkFlags = [Bool]()
    var isAddStatus: Bool = true
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tbNutritionList.tableFooterView = UIView()
        /*
        boxView.layer.borderWidth = 2.0
        boxView.layer.borderColor = UIColor.white.cgColor
        boxView.clipsToBounds = true
        boxView.layer.cornerRadius = 10
        boxView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        */

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.showTopView()
        
        myMealList.removeAll()
        checkFlags.removeAll()
        
        //  load saved my meal list
        myMealList = getMyMeal()
        
        //  init flag as false
        for _ in myMealList {
            checkFlags.append(false)
        }
        
        tbNutritionList.reloadData()
        
    }
    
    @IBAction func gotoAddAction(_ sender: Any) {
        
        let createMealVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateMealViewController") as! CreateMealViewController
        self.navigationController?.pushViewController(createMealVC, animated: true)
    }
    
    @objc func addMeal(_ sender: UIButton) {
        
        if checkFlags[sender.tag] {
            
            self.showAlertDialog(title: Const.AppName, message: "Food is added already", positive: "OK", negative: nil)            
            return
            
        }
        
        checkFlags[sender.tag] = !checkFlags[sender.tag]
        
        let myMeal = myMealList[sender.tag]

        let nutrition =  NutritionModel()
        
        let calendar = Calendar.current
        let dateComponent = calendar.dateComponents([.year, .month, .day], from: SelectedDay)
        let curDate = calendar.date(from: dateComponent)
        
        let historyModel = HistoryModel(foodId: myMeal.id,
                                        foodName: myMeal.name,
                                        carbs: myMeal.nutrition.map( {$0.carbs} ).reduce(0, +),
                                        calories: myMeal.nutrition.map( {$0.calories} ).reduce(0, +),
                                        fat: myMeal.nutrition.map( {$0.fat} ).reduce(0, +),
                                        protein: myMeal.nutrition.map( {$0.protein} ).reduce(0, +),
                                        addDate: curDate!)

        
        nutrition.calories = getcurrentNutrition().calories + Float(myMeal.nutrition.map( {$0.calories} ).reduce(0, +))
        nutrition.carbs = getcurrentNutrition().carbs + myMeal.nutrition.map( {$0.carbs} ).reduce(0, +)
        nutrition.fat = getcurrentNutrition().fat + myMeal.nutrition.map( {$0.fat} ).reduce(0, +)
        nutrition.protein = getcurrentNutrition().protein + myMeal.nutrition.map( {$0.protein} ).reduce(0, +)
        
        // Save history
        HistoryList.append(historyModel)
        
        saveHistory(HistoryList)

        currentNutrition = nutrition

        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.setValues()
        
        tbNutritionList.reloadData()
        
        self.showToast("Food is added")
        
        
        
    }
    
    
    @objc func deleteMeal(_ sender: UIButton) {
        
        self.showAlert(title: Const.AppName, message: Const.MSG_DELETE, okButtonTitle: "Yes", cancelButtonTitle: "No") {
            
            self.myMealList.remove(at: sender.tag)
            self.checkFlags.remove(at: sender.tag)
            
            self.tbNutritionList.reloadData()
            
            // Save data to meal
            
            saveMyMeals(self.myMealList)

        }
        
    }
    
    
    //MARK: - Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myMealList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MealCell") as! MealCell
        cell.lblName.text = myMealList[indexPath.row].name
        cell.btnAdd.tag = indexPath.row
        cell.btnDelete.tag = indexPath.row
        cell.btnAdd.addTarget(self, action: #selector(addMeal(_:)), for: .touchUpInside)
        cell.btnDelete.addTarget(self, action: #selector(deleteMeal(_ :)), for: .touchUpInside)
        
        //if checkFlags[indexPath.row] {
            cell.imvAdd.image = UIImage.init(named: "add")
        //} else {
        //    cell.imvAdd.image = UIImage.init(named: "ic_unchecked")
        //}
        
        return cell
    }
}
