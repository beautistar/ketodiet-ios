//
//  BarcodeViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/21/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import RSBarcodes_Swift

class BarcodeViewController: RSCodeReaderViewController {
    
    @IBOutlet var toggle: UIButton!
    
    var foodVC: FoodViewController?
    var mealVC: CreateMealViewController?
    var barcode: String = ""
    var dispatched: Bool = false
    var from = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // MARK: NOTE: Uncomment the following line to enable crazy mode
        // self.isCrazyMode = true
        
        self.focusMarkLayer.strokeColor = UIColor.red.cgColor
        
        self.cornersLayer.strokeColor = UIColor.yellow.cgColor
        
        self.tapHandler = { point in
            print(point)
        }
        
        // MARK: NOTE: If you want to detect specific barcode types, you should update the types
        let types = self.output.availableMetadataObjectTypes
        // MARK: NOTE: Uncomment the following line remove QRCode scanning capability
        // types = types.filter({ $0 != AVMetadataObject.ObjectType.qr })
        self.output.metadataObjectTypes = types
        
        // MARK: NOTE: If you layout views in storyboard, you should these 3 lines
        for subview in self.view.subviews {
            self.view.bringSubviewToFront(subview)
        }
        
        self.toggle.isEnabled = self.hasTorch()
        
        self.barcodesHandler = { barcodes in
            if !self.dispatched { // triggers for only once
                self.dispatched = true
                for barcode in barcodes {
                    guard let barcodeString = barcode.stringValue else { continue }
                    self.barcode = barcodeString
                    print("Barcode found: type=" + barcode.type.rawValue + " value=" + barcodeString)
                    
                    DispatchQueue.main.async(execute: {
                        //self.performSegue(withIdentifier: "nextView", sender: self)
                        self.showLoadingViewWithTitle(title: "Finding food...")
                        
                        ApiRequest.barcode(barcodeString, completion: { (resCode, food_id) in
                            
                            self.hideLoadingView()
                            
                            if resCode == Const.CODE_SUCCESS {
                                if food_id == 0 {
                                    self.showAlertDialog(title: Const.AppName, message: Const.MSG_NOTFOUND, positive: "OK", negative: nil)
                                    self.dismiss(animated: true, completion: nil)
                                    
                                } else {
                                    
                                    ApiRequest.getFood("\(food_id)", from: Const.FROM_BARCODE, completion: { (resCode, food) in
                                        
                                        if resCode == Const.CODE_SUCCESS {
                                            
                                            self.dismissVC(food)
                                            
                                        } else {
                                            self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
                                            self.dismiss(animated: true, completion: nil)
                                        }
                                    })
                                }
                            } else {
                                self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
                                self.dismiss(animated: true, completion: nil)
                            }
                        })
                    })
                    
                    // MARK: NOTE: break here to only handle the first barcode object
                    break
                }
            }
        }
    }
    
    func dismissVC(_ food: FoodModel) {

        if from == Const.FROM_FOODLIST {
            
            self.dismiss(animated: true, completion: {
                self.foodVC?.dismissProcess(food)
            })
            
        } else {
            self.dismiss(animated: true, completion: {
                self.mealVC?.dismissProcess(food)
            })
        }
        
    }
    
    @IBAction func toggle(_ sender: AnyObject?) {
        let isTorchOn = self.toggleTorch()
        print(isTorchOn)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.dispatched = false // reset the flag so user can do another scan
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
}


extension BarcodeViewController {
    
    func showAlertDialog(title: String!, message: String!, positive: String?, negative: String?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (positive != nil) {
            
            alert.addAction(UIAlertAction(title: positive, style: .default, handler: nil))
        }
        
        if (negative != nil) {
            
            alert.addAction(UIAlertAction(title: negative, style: .default, handler: nil))
        }
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func showLoadingView() {
        
        showLoadingViewWithTitle(title: "")
    }
    
    func showLoadingViewWithTitle(title: String) {
        
        if title == "" {
            
            ProgressHUD.show()
            
        } else {
            
            ProgressHUD.showWithStatus(string: title)
        }
    }
    
    // hide loading view
    func hideLoadingView() {
        
        ProgressHUD.dismiss()
    }
    
}
