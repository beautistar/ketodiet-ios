//
//  HomeViewController.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import UICircularProgressRing
import Presentr
import GoogleMobileAds

class HomeViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var imvBg: UIImageView!
    @IBOutlet weak var progressView: UICircularProgressRing!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var tblFoodList: UITableView!
   
    @IBOutlet weak var lblCarbs: UILabel!
    @IBOutlet weak var lblCalories: UILabel!
    @IBOutlet weak var lblProtein: UILabel!
    @IBOutlet weak var lblFat: UILabel!
    
    @IBOutlet weak var progCalories: UIProgressView!
    @IBOutlet weak var progProtein: UIProgressView!
    @IBOutlet weak var progFat: UIProgressView!
    
    @IBOutlet weak var lblToday: UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()

    var foodList = [HistoryModel]()
    var selectedFood = FoodModel()
    var selectedFoodHistory: HistoryModel?
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load bannerView view.
        
        foodAddCount = 0
        
        if UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
            
            bannerView.isHidden = true
            
        } else {
            
            bannerView.adUnitID = Const.bannerID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())            
            
        }

        
        SelectedDay = Date()
        
        
        setNavigationBarItem()
        
        self.title = "Dashboard"
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.colorTop
        
        // add copy button
        let copybtn: UIBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "ic_copy"), style: .done, target: self, action: #selector(copyAction))
        self.navigationItem.rightBarButtonItem = copybtn
        
        /*
        imvBg.clipsToBounds = true
        imvBg.layer.cornerRadius = 10
        imvBg.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        */
        infoView.clipsToBounds = true
        infoView.layer.cornerRadius = 10
        infoView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMaxYCorner]
 
        
        // shadow effect
        plusButton.layer.shadowColor = UIColor.black.cgColor
        plusButton.layer.shadowOpacity = 0.5
        plusButton.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
        plusButton.layer.shadowRadius = 5
        
        
        // load saved food history
        HistoryList = getHistory()
        // load saved recent food history
        RecentHistoryList = getRecent()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setValues()
        
        loadFoodList(SelectedDay, from: Const.FROM_DATE)
    }
    
    func setValues() {
        
        // check viewed or not
        if UserDefaults.standard.bool(forKey: Const.KEY_VIEWED) {
            tblFoodList.isHidden = false
        } else {
            tblFoodList.isHidden = true
        }
        
        
        let savedNutrition = getcurrentNutrition()
        let nutritionHelper = NutritionGoalsHelper(user: getCurrentUser())

        lblCarbs.text = String(format: "%.1fg / %.1fg", savedNutrition.carbs, nutritionHelper.getNetCarbs())
        lblCalories.text = String(format: "%.1fkcal / %.1fkcal", savedNutrition.calories, nutritionHelper.getCalories())
        lblFat.text = String(format: "%.1fg / %.1fg", savedNutrition.fat, nutritionHelper.getFat())
        lblProtein.text = String(format: "%.1fg / %.1fg", savedNutrition.protein,  nutritionHelper.getProtein())
        
        progressView.maxValue = UICircularProgressRing.ProgressValue(nutritionHelper.getNetCarbs())
        progressView.value = UICircularProgressRing.ProgressValue(savedNutrition.carbs)
        progressView.innerRingColor = UIColor.white
        //progressView.showFloatingPoint = true
        //progressView.decimalPlaces = 1
        //progressView.valueIndicator = String(format: "g / %.1fg", nutritionHelper.getNetCarbs())
        progressView.startProgress(to: UICircularProgressRing.ProgressValue(savedNutrition.carbs), duration: 2.0)
        
        // for test
        UIView.animate(withDuration: 2.0) {
            self.progCalories.setProgress(Float(savedNutrition.calories) / nutritionHelper.getCalories(), animated: true)
            self.progProtein.setProgress(savedNutrition.protein / nutritionHelper.getProtein(), animated: true)
            self.progFat.setProgress(savedNutrition.fat / nutritionHelper.getFat(), animated: true)
            //self.view.layoutIfNeeded()
        }
    }
    
    
    func loadFoodList(_ date: Date, from: Int) {
        
        self.foodList.removeAll()
        
        let calendar = Calendar.current
        let dateComponent = calendar.dateComponents([.year, .month, .day], from: date)
        let selDate = calendar.date(from: dateComponent)
        
        let todayComponent = calendar.dateComponents([.year, .month, .day], from: Date())
        let todayDate = calendar.date(from: todayComponent)
        
        
        let selectedList = HistoryList.filter( {$0.addDate == selDate })
        self.foodList = selectedList
        
        //MARK: - NOTE: update current nutrition to sum of history nutirtion and updates charts
        
        let nutrition = NutritionModel()
        nutrition.carbs = self.foodList.map( {$0.carbs} ).reduce(0, +)
        nutrition.calories = Float(self.foodList.map( {$0.calories} ).reduce(0, +))
        nutrition.fat = self.foodList.map( {$0.fat} ).reduce(0, +)
        nutrition.protein = self.foodList.map( {$0.protein} ).reduce(0, +)
        
        currentNutrition = nutrition
        
        lblToday.text = "Today"
        
        if from == Const.FROM_DATE {
            
            if selDate != todayDate {
                // todayDate    Date?    2018-11-02 04:00:00 UTC    some
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                lblToday.text = formatter.string(from: selDate!)
            }
            
        } else { // COPY case
            
            if selectedList.count > 0 {
                
                var newList = HistoryList.filter( {$0.addDate != todayDate} )
                
                for one in selectedList {
                    
                    let historyModel = HistoryModel(foodId: one.foodId, foodName: one.foodName, carbs: one.carbs, calories: one.calories, fat: one.fat, protein: one.protein, addDate: todayDate!)
                    
                    newList.append(historyModel)
                    
                }                
                
                HistoryList = newList
                
                saveHistory(HistoryList)
                
            } else {
                
                self.showToast(Const.MSG_NOFOOD)
                let selectedList = HistoryList.filter( {$0.addDate == todayDate })
                self.foodList = selectedList
            }            
            
            SelectedDay = Date()
            
        }
        
        
        self.setValues()
        
        self.tblFoodList.reloadData()
        
    }
    
    func createAndLoadInsterstitial() {
        
        interstitial = GADInterstitial(adUnitID: Const.interstitialID)
        let request = GADRequest()
        // Request test ads on devices you specify. Your test device ID is printed to the console when
        // an ad request is made.
        request.testDevices = [ kGADSimulatorID, "2077ef9a63d2b398840261c8221a0c9a" ]
        interstitial.load(request)
    }
    
    
    @objc func copyAction(_ sender: Any) {
        
        self.showToast("Copy daily intakes from a previous day")
        
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        calendarVC.homeVC = self
        calendarVC.from = Const.FROM_COPY
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 590), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: calendarVC, animated: true, completion: nil)
        
    }
    
    
    @IBAction func calendarAction(_ sender: Any) {
        
        let calendarVC = self.storyboard?.instantiateViewController(withIdentifier: "CalendarViewController") as! CalendarViewController
        calendarVC.homeVC = self
        calendarVC.from = Const.FROM_DATE
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 590), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: calendarVC, animated: true, completion: nil)        
    }
    
    
    @IBAction func plusAction(_ sender: Any) {
            
        if !UserDefaults.standard.bool(forKey: Const.KEY_VIEWED) {
            UserDefaults.standard.set(true, forKey: Const.KEY_VIEWED)
        }
        
        
        // check add food count & show interstitial ads
        
        if foodAddCount > 1 && !UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
            
            self.showInterstitalAds()
        
        } else if foodAddCount == 1  {
            
            gotoAddNutrition()
            
            if !UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
                createAndLoadInsterstitial()
            }
            
            
        } else {
            
            gotoAddNutrition()
        
        }
        
        foodAddCount = foodAddCount + 1

    }

    func showInterstitalAds() {
        
        if interstitial.isReady {
            
            interstitialCount = interstitialCount + 1
            
            if interstitialCount > 3 {
                
                interstitialCount = 0
                
                self.showAlert(title: Const.AppName, message: Const.MSG_REMOVE_ADS, okButtonTitle: "Yes", cancelButtonTitle: "No") {
                    
                    let purchaseVC = self.storyboard?.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
                    self.navigationController?.pushViewController(purchaseVC, animated: true)
                }
                
            } else {
                
                interstitial.present(fromRootViewController: self)
                foodAddCount = 0
            }
            
        } else {
            print("Ad wasn't ready")
        }
    }

    
    func gotoAddNutrition() {
        
        let addNutritionVC = self.storyboard?.instantiateViewController(withIdentifier: "AddNutritionViewController") as! AddNutritionViewController
        self.navigationController?.pushViewController(addNutritionVC, animated: true)
    }
    
    
    func presentAddVC() {
        
        let addConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmViewController") as! AddConfirmViewController
        addConfirmVC.selectedFood = self.selectedFood
        addConfirmVC.selectedHistory = self.selectedFoodHistory
        addConfirmVC.homeVC = self
        addConfirmVC.from = Const.FROM_HOME
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 450), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: addConfirmVC, animated: true, completion: nil)
    }
    
    
    @objc func deleteAction(_ sender: UIButton) {
        
        self.showAlert(title: Const.AppName, message: "Are you sure want to delete?", okButtonTitle: "Yes", cancelButtonTitle: "No") {
            
            self.selectedFoodHistory = self.foodList[sender.tag]
            
            
            if let index = HistoryList.index(where: { $0 == self.selectedFoodHistory }) {
                
                HistoryList.remove(at: index)
                
                let userDefaults = UserDefaults.standard
                let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: HistoryList)
                userDefaults.set(encodedData, forKey: Const.KEY_HISTORY)
                userDefaults.synchronize()
                
            }
 
            let nutrition = NutritionModel()
            
            nutrition.calories = getcurrentNutrition().calories - Float(self.selectedFoodHistory!.calories)
            nutrition.carbs = getcurrentNutrition().carbs - self.selectedFoodHistory!.carbs
            nutrition.fat = getcurrentNutrition().fat - self.selectedFoodHistory!.fat
            nutrition.protein = getcurrentNutrition().protein - self.selectedFoodHistory!.protein
            
            currentNutrition = nutrition
            self.setValues()
            
            self.foodList.remove(at: sender.tag)
            self.tblFoodList.reloadData()
            
        }
    }
    
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return foodList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell") as! FoodCell
        
        let selectedFoodHistory = foodList[indexPath.row]
        
        cell.lblTitle.text = selectedFoodHistory.foodName
        cell.lblContents.text = String(format: "%.1fg Carbs - %.1fkcal - %.1fg Fat - %.1fg Protein", selectedFoodHistory.carbs, Float(selectedFoodHistory.calories), selectedFoodHistory.fat, selectedFoodHistory.protein)
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteAction(_ :)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedFoodHistory = foodList[indexPath.row]
        
        self.showLoadingView()
        ApiRequest.getFood(selectedFoodHistory!.foodId, from: Const.FROM_FOODLIST) { (resCode, food) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                
                self.selectedFood = food
                
                if self.selectedFood.servings.count > 0 {
                    self.presentAddVC()
                } /*else {
                    self.showAlertDialog(title: Const.AppName, message: "Can't find food macros", positive: "OK", negative: nil)
                }*/
                
            } else if resCode == Const.CODE_105 {
                
                //self.showAlertDialog(title: Const.AppName, message: "Can't find food macros", positive: "OK", negative: nil)
            } else {
                self.showAlertDialog(title: Const.AppName, message: Const.MSG_ERROR, positive: "OK", negative: nil)
            }
        }
    }
}

extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
