//
//  RecentViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import Presentr

class RecentViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tbNutritionList: UITableView!
    @IBOutlet weak var boxView: UIView!
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    var recentList = [HistoryModel]()
    var selectedFood = FoodModel()
    var selectedHistory: HistoryModel?
    

    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        topView.isHidden = true
        
        tbNutritionList.tableFooterView = UIView()
        
        boxView.layer.borderWidth = 2.0
        boxView.layer.borderColor = UIColor.white.cgColor
        boxView.clipsToBounds = true
        boxView.layer.cornerRadius = 10
        boxView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]       
        
    }    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.showTopView()
        
        let reversedList = RecentHistoryList.reversed()
        if reversedList.count > 5 {
            recentList = Array(reversedList.prefix(5))
            
        } else {
            recentList = Array(reversedList)
        }
        
        tbNutritionList.reloadData()
    }
    
    
    @objc func addRecent(_ sender: UIButton) {
        
        selectedHistory = recentList[sender.tag]
        
        self.showLoadingView()
        ApiRequest.getFood(selectedHistory!.foodId, from: Const.FROM_RECENT) { (resCode, food) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                
                self.selectedFood = food
                self.presentAddVC()
            }
        }
    }
    
    
    func presentAddVC() {
        
        let addConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmViewController") as! AddConfirmViewController
        addConfirmVC.selectedFood = self.selectedFood
        addConfirmVC.selectedHistory = self.selectedHistory
        addConfirmVC.from = Const.FROM_RECENT
        addConfirmVC.addNutritionVC = self.parent?.parent?.parent as? AddNutritionViewController
        addConfirmVC.homeVC = self.parent?.parent?.parent?.parent as? HomeViewController

        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 450), center: ModalCenterPosition.center)
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: addConfirmVC, animated: true, completion: nil)
        
    }
    
    // MARK: - TableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell") as! FoodCell
        cell.lblTitle.text = recentList[indexPath.row].foodName
        cell.lblContents.text = String(format: "%.1fg Carbs - %.1fkcal - %.1fg Fat - %.1fg Protein", recentList[indexPath.row].carbs, Float(recentList[indexPath.row].calories), recentList[indexPath.row].fat, recentList[indexPath.row].protein)
        cell.addButton.tag = indexPath.row
        cell.addButton.addTarget(self, action: #selector(addRecent), for: .touchUpInside)
        
        return cell
    }

}
