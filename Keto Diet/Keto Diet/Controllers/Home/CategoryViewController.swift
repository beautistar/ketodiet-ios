//
//  CategoryViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/15/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import SwiftyJSON
import Presentr

enum Status {
    case category
    case product
    case item
}

class CategoryViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tbNutritionList: UITableView!
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var tfFood: UITextField!
    
    fileprivate var showList : [String] = []
    fileprivate var rawCategories: JSON = JSON()
    fileprivate var status: Status = .category
    
    fileprivate var selectedCategory = ""
    fileprivate var selectedProduct = ""
    fileprivate var selectedItem = ""
    fileprivate var isCategoryList: Bool = true
    
    
    let presenter: Presentr = {
        let presenter = Presentr(presentationType: .alert)
        presenter.transitionType = TransitionType.coverHorizontalFromRight
        presenter.dismissOnSwipe = true
        return presenter
    }()
    
    var foodList = [FoodModel]()
    var selectedFood = FoodModel()
    var selectedFoodId: String = ""
    var selectedServing = ServingModel()
    
    
    
    //MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tbNutritionList.tableFooterView = UIView()
        
        boxView.layer.borderWidth = 2.0
        boxView.layer.borderColor = UIColor.white.cgColor
        boxView.clipsToBounds = true
        boxView.layer.cornerRadius = 10
        boxView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        topView.isHidden = true
        
        
        
        loadJSON()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let parentVC = self.parent?.parent?.parent as? AddNutritionViewController
        parentVC?.showTopView()
        
        isCategoryList = true
        
        tbNutritionList.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        getCategory()
    }
    
    private func loadJSON() {
        if let path = Bundle.main.path(forResource: "keto_categories", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let json = try JSON(data: data)
                rawCategories = json[Const.PARAM_CATEGORIES]

            } catch {
                
            }
        }
    }

    private func getCategory() {
        
        status = .category
        
        self.showList = rawCategories.arrayValue.compactMap { $0.dictionaryObject?.keys.first }
        
        tbNutritionList.reloadData()

    }
    
    private func getProducts() {
        
        status = .product
        
        guard let subCat = rawCategories.arrayValue.first ( where: { $0.dictionaryValue.keys.first == selectedCategory }) else { return }
        
        self.showList = subCat[selectedCategory].arrayValue.compactMap { $0.dictionaryObject?.keys.first }
        
        tbNutritionList.reloadData()
      
    }
    
    private func getItem() {
        
        status = .item
        
        guard let subCat = rawCategories.arrayValue.first ( where: { $0.dictionaryValue.keys.first == selectedCategory }) else { return }
        guard let products = subCat[selectedCategory].arrayValue.first ( where: { $0.dictionaryValue.keys.first == selectedProduct }) else { return }
        guard let items = products[selectedProduct].arrayObject as? [String] else { return }
        
        self.showList = items
        
        tbNutritionList.reloadData()
    }
    
    private func selectItem(_ item: String) {
        // TODO: Here
        
        print("Selected item: " , item)
        
        tfFood.text = item
        
        self.showLoadingView()
        
        ApiRequest.foodSearch(item) { (resCode, foods) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                
                self.foodList = foods
                
                self.isCategoryList = false
                
                self.tbNutritionList.reloadData()
            }
        }
        
        
    }
    
    @objc func getFoodAction(_ sender: Any) {
        
        let btn = sender as! UIButton
        selectedFoodId = foodList[btn.tag].id
        
        getFood()
    }
    
    func getFood() {
        
        self.showLoadingView()
        
        ApiRequest.getFood(selectedFoodId, from: Const.FROM_FOODLIST) { (resCode, food) in
            
            self.hideLoadingView()
            
            if resCode == Const.CODE_SUCCESS {
                self.selectedFood = food
                self.presentAddVC()
            }
        }
    }
    
    
    func presentAddVC() {
        
        let addConfirmVC = self.storyboard?.instantiateViewController(withIdentifier: "AddConfirmViewController") as! AddConfirmViewController
        addConfirmVC.selectedFood = self.selectedFood
        addConfirmVC.addNutritionVC = self.parent?.parent?.parent as? AddNutritionViewController
        addConfirmVC.homeVC = self.parent?.parent?.parent?.parent as? HomeViewController
        addConfirmVC.from = Const.FROM_FOODLIST
        
        presenter.presentationType = .custom(width: ModalSize.fluid(percentage: 0.9), height: ModalSize.custom(size: 450), center: ModalCenterPosition.center)
        
        presenter.transitionType = nil
        presenter.dismissTransitionType = nil
        presenter.dismissAnimated = true
        customPresentViewController(presenter, viewController: addConfirmVC, animated: true, completion: nil)
    }
    
    
    //MARK: - TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isCategoryList {
            
            return showList.count

        } else {
            
            return foodList.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isCategoryList {
            
            let cell = UITableViewCell(style: .default, reuseIdentifier: "FoodListCell")
            cell.textLabel?.textColor = UIColor.gray
            cell.textLabel?.text = showList[indexPath.row]
            cell.backgroundColor = UIColor.clear
            
            tableView.separatorStyle = .singleLine
            
            return cell
            
        }
        
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "FoodCell") as! FoodCell
            cell.addButton.tag = indexPath.row
            cell.addButton.addTarget(self, action: #selector(getFoodAction(_:)), for: .touchUpInside)
            cell.lblTitle.text = foodList[indexPath.row].name
            
            tableView.separatorStyle = .none
            
            return cell
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isCategoryList {
            
            switch status {
            case .category:
                selectedCategory = showList[indexPath.row]
                getProducts()
                break
                
            case .product:
                selectedProduct = showList[indexPath.row]
                getItem()
                break
                
            case .item:
                selectedItem = showList[indexPath.row]
                selectItem(selectedItem)
                break
            }
            
        }
        
    }

}
