//
//  MoreViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/20/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import GoogleMobileAds

class MoreViewController: UIViewController {

    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
            
            bannerView.isHidden = true
            
        } else {
            
            bannerView.adUnitID = Const.bannerID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
        }
        
        setNavigationBarItem()
        
        self.title = "More Apps"
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.colorTop
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let url = URL(string: "https://itunes.apple.com/us/developer/smart-query/id1316179292")
        if let unwrappedURL = url {
            
            let request = URLRequest(url: unwrappedURL)
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if error == nil {
                    
                    self.webView.loadRequest(request)
                    
                } else {
                    
                    print("ERROR: \(error!)")
                    
                }
                
            }
            
            task.resume()
            
        }
    }
}
