//
//  PrivacyViewController.swift
//  Keto Diet
//
//  Created by Developer on 10/20/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit
import GoogleMobileAds

class PrivacyViewController: UIViewController {

    @IBOutlet weak var bannerView: GADBannerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if UserDefaults.standard.bool(forKey: Const.KEY_ISPURCHASED) {
            
            bannerView.isHidden = true
            
        } else {
            
            bannerView.adUnitID = Const.bannerID
            bannerView.rootViewController = self
            bannerView.load(GADRequest())
            
        }
        
        setNavigationBarItem()
        
        self.title = "Privacy Policy"
        self.navigationController!.navigationBar.isTranslucent = false
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.barTintColor = Const.colorTop
    }

}
