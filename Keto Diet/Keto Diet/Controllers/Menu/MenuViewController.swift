//
//  MenuViewController.swift
//  Ride
//
//  Created by Yin on 2018/9/4.
//  Copyright © 2018 Yin. All rights reserved.
//

import UIKit

//import SDWebImage

enum LeftMenu: Int {
    case main = 0
    case setting
    case about
    case rate
    case share
    case more
    case privacy
    
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, LeftMenuProtocol {

    @IBOutlet weak var tblMenu: UITableView!
    @IBOutlet weak var imvBg: UIImageView!
   
    var menusNames = ["Dashboard", "Settings", "About", "Rate", "Share", "More apps", "Privacy Policy"]
    var menuIcons = [#imageLiteral(resourceName: "ic_home"), #imageLiteral(resourceName: "ic_settings"), #imageLiteral(resourceName: "ic_information"), #imageLiteral(resourceName: "ic_star"), #imageLiteral(resourceName: "ic_share"), #imageLiteral(resourceName: "ic_more"), #imageLiteral(resourceName: "ic_privacy")]
    
    var homeViewController: UIViewController!
    var settingViewController: UIViewController!
    var informationViewController: UIViewController!
    var moreViewController: UIViewController!
    var privacyViewController: UIViewController!
    
    var webView = UIWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initView() {
        
        tblMenu.tableFooterView = UIView()
        
        let homeVC = storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.homeViewController = UINavigationController(rootViewController: homeVC)
        
        let settingsVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController") as! SettingViewController
        self.settingViewController = UINavigationController(rootViewController: settingsVC)
        
        let infoVC = self.storyboard?.instantiateViewController(withIdentifier: "InfomationViewController") as! InfomationViewController
        self.informationViewController = UINavigationController(rootViewController: infoVC)
        
        let moreVC = self.storyboard?.instantiateViewController(withIdentifier: "MoreViewController") as! MoreViewController
        self.moreViewController = UINavigationController(rootViewController: moreVC)
        
        let privacyVC = self.storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        self.privacyViewController = UINavigationController(rootViewController: privacyVC)
        
        /*
        imvBg.clipsToBounds = true
        imvBg.layer.cornerRadius = 10
        imvBg.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        */

    }
    
    // MARK: - left menu protocol
    func changeViewController(_ menu: LeftMenu) {
        
        switch menu {
            
        case .main:
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
            
        case .setting:
            self.slideMenuController()?.changeMainViewController(self.settingViewController, close: true)
           
        case .about:
            self.slideMenuController()?.changeMainViewController(self.informationViewController, close: true)

        case .rate:
            rateApp(appId: Const.AppID) { (success) in
                print("RateApp \(success)")
            }
            self.closeLeft()
        case .share:
            share(self.view)
            
        case .more:
            
            showMore()
            self.closeLeft()
            //self.slideMenuController()?.changeMainViewController(self.moreViewController, close: true)
            
        case .privacy:
            self.slideMenuController()?.changeMainViewController(self.privacyViewController, close: true)
        }
    }
    
    func logout() {
        
        //TODO: logoout action
        
        let loginNav = self.storyboard?.instantiateViewController(withIdentifier: "LoginNav") as! UINavigationController
        UIApplication.shared.keyWindow?.rootViewController = loginNav
    }
    
    func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: completion)
    }
    
    func share(_ sender: Any) {
        
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [Const.AppURL], applicationActivities: nil)
        
        // This lines is for the popover you need to show in iPad
        activityViewController.popoverPresentationController?.sourceView = (sender as! UIView)
        
        // This line remove the arrow of the popover to show in iPad
        activityViewController.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)
        
        // Anything you want to exclude
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        
        self.present(activityViewController, animated: true, completion: {
            self.closeLeft()
        })
    }
    
    func showMore() {
        
        let url = URL(string: "https://itunes.apple.com/us/developer/smart-query/id1316179292")
        if let unwrappedURL = url {
            
            let request = URLRequest(url: unwrappedURL)
            let session = URLSession.shared
            
            let task = session.dataTask(with: request) { (data, response, error) in
                
                if error == nil {
                    
                    self.webView.loadRequest(request)
                    
                } else {
                    
                    print("ERROR: \(error!)")
                    
                }
                
            }
            
            task.resume()
            
        }
    }
    
}
//MARK: - Extension
extension MenuViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .setting, .about, .rate, .share, .more, .privacy:
                return 50
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tblMenu == scrollView {
            
        }
    }
}

extension MenuViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menusNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let menu = LeftMenu(rawValue: indexPath.row) {
            switch menu {
            case .main, .setting, .about, .rate, .share, .more, .privacy:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell") as! MenuCell
                cell.imvIcon.image = menuIcons[indexPath.row]
                cell.lblName.text = menusNames[indexPath.row]
                return cell
            }
        }
        return UITableViewCell()
    }
}
