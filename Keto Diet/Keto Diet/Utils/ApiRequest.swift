//
//  ApiRequest.swift
//  Keto Diet
//
//  Created by Developer on 10/23/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON

class ApiRequest {
    
    static let SERVER_URL                           = "http://ketoapi.sqmapps.com/"
    static let MARK_Q                               = "?"
    
    static let REQ_FOOD_SEARCH                      = SERVER_URL + "food_search.php"
    static let REQ_AUTO_COMPLETE                    = SERVER_URL + "autocomplete.php"
    static let REQ_BARCODE                          = SERVER_URL + "barcode.php"
    static let REQ_GET_FOOD                         = SERVER_URL + "getfood.php"
    
    
    
    static func autoComplete(_ name: String, completion: @escaping(Int, [String]) -> ()) {
        
        let params = [Const.PARAM_Q: name] as [String : Any]
        
        Alamofire.request(REQ_AUTO_COMPLETE, method: .get, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                
                var suggestion_list = [String]()
                
                let suggestions = json["suggestions"]["suggestion"].arrayValue
                    
                for suggestion in suggestions {
                    suggestion_list.append(suggestion.stringValue)
                }
                completion(Const.CODE_SUCCESS, suggestion_list)
                
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func foodSearch(_ name: String, completion: @escaping(Int, [FoodModel]) -> ()) {
        

        let params = [Const.PARAM_Q: name] as [String : Any]
        
        Alamofire.request(REQ_FOOD_SEARCH, method: .get, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                
                let foods = json[Const.PARAM_FOODS][Const.PARAM_FOOD].arrayValue
                
                var food_list = [FoodModel]()
                
                for food_object in foods {
                    let food = ParseHelper.parseFood(food_object)
                    food_list.append(food)
                }
                completion(Const.CODE_SUCCESS, food_list)
                
            } else {
                completion(Const.CODE_FAIL, [])
            }
        }
    }
    
    static func getFood(_ food_id: String, from: Int, completion: @escaping(Int, FoodModel) -> ()) {
        
        
        let params = [Const.PARAM_FOOD: food_id] as [String : Any]
        
        Alamofire.request(REQ_GET_FOOD, method: .get, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                
                var error = 0
                error = json["error"]["code"].intValue                
                if error != 0 {
                    completion(Const.CODE_105, FoodModel())
                    return
                }                
                
                var food: FoodModel!
                if from == Const.FROM_BARCODE {
                    food = ParseHelper.parseFoodServingOne(json)
                } else {
                    food = ParseHelper.parseFoodServings(json)
                }
                
                completion(Const.CODE_SUCCESS, food)
                
            } else {
                completion(Const.CODE_FAIL, FoodModel())
            }
        }
    }
    
    static func barcode(_ barcode: String, completion: @escaping(Int, Int) -> ()) {
        
        
        let params = [Const.PARAM_BARCODE: barcode] as [String : Any]
        
        Alamofire.request(REQ_BARCODE, method: .get, parameters: params).responseJSON { response in
            
            print(response)
            
            if response.result.isSuccess {
                
                let json = JSON(response.result.value!)
                
                let food_id = json[Const.PARAM_FOOD_ID][Const.PARAM_VALUE].intValue
                
                completion(Const.CODE_SUCCESS, food_id)
                
            } else {
                completion(Const.CODE_FAIL, 0)
            }
        }
    }

}
