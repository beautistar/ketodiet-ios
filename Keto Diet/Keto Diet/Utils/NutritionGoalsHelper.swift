//
//  NutritionGoalsHelper.swift
//  Keto Diet
//
//  Created by Developer on 10/21/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class NutritionGoalsHelper {
    
    var BMR: Float = 0
    var TDEEkcal: Float = 0
    var TEF: Float = 0
    var DEFAULT_HEURISTIC_COEFFICIENT: Float = 0.1
    
    var activeLevelIndexSettings : Int = 0
    var activeLevelValues: [Float] = [1.15, 1.3, 1.5, 1.7, 2.0]
    var calories: Float = 0
    var caloriesMax: Float = 0
    var caloriesMin: Float = 0
    var fat: Float = 0
    var fatC: Float = 0
    var fatMax: Float = 0
    var fatMin: Float = 0
    var fatP: Float = 0
    var fatSettings: Float = 0
    
    var goalCaloriesMaxValues: [Float] = [1.0, 1.1, 1.15]
    var goalCaloriesMinValues: [Float] = [0.7, 0.85, 1.0]
    var goalIndexSettings: Int = 0
    var goalProteinMaxValues: [Float] = [1.5, 1.5, 1.5]
    var goalProteinMinValues: [Float] = [0.4, 0.4, 0.4]
    var goalValuesCalories: [Float] = [0.78, 1.0, 1.12]
    var goalValuesProtein: [Float] = [0.6, 0.8, 1.0]
    var netCarbs: Float = 0
    var netCarbsC: Float = 0
    var netCarbsMax: Float = 0
    var netCarbsMin: Float = 0
    var netCarbsP: Float = 0
    var netWeightKG: Float = 0
    var netWeightLbs: Float = 0
    var netWeightStart: Float = 0
    var protein: Float = 0
    var proteinC: Float = 0
    var proteinMax: Float = 0
    var proteinMin: Float = 0
    var proteinP: Float = 0
    var sumBMRandTEF: Float = 0
    var weightFromSettings: Float = 0
    
    init(user: UserModel) {
        
        //MARK: NOTE: replace with current user information        
            
        fatSettings = user.fat
        activeLevelIndexSettings = user.activityLevel
        goalIndexSettings = user.goal
        weightFromSettings = user.weight
        
        let typeWeight = user.weightType
        netWeightStart = weightFromSettings
        
        if typeWeight == Const.TYPE_WEIGHT_KG {
            netWeightStart = weightFromSettings * 2.20462
        }
        
        netWeightLbs = netWeightStart
        let f: Float = (netWeightStart * Float(100 - fatSettings)) / 100.0
        netWeightKG = f
        netWeightLbs = f
        netWeightKG *= 0.453592
        BMR = (netWeightKG * 21.6 ) + 370.0
        TEF = BMR * DEFAULT_HEURISTIC_COEFFICIENT
        sumBMRandTEF = BMR + TEF
        TDEEkcal = sumBMRandTEF * activeLevelValues[activeLevelIndexSettings]
        calories = Float(round(TDEEkcal * goalValuesCalories[goalIndexSettings]))
        var fatDiff: Float = 0.0
        netCarbs = Float(round((calories * 0.05) / 4.0))
        if round(netCarbs) > 50 {
            fatDiff = (netCarbs - 50.0) / 2.5
            netCarbs = 50.0
        }
        netCarbsC = netCarbs * 4.0
        netCarbsP = Float(round((netCarbsC / calories) * 100.0))
        protein = Float(round(goalValuesProtein[goalIndexSettings] * netWeightLbs))
        proteinC = protein * 4.0
        proteinP = Float(round((proteinC / calories) * 100.0))
        fatC = (calories - netCarbsC) - proteinC
        fat = Float(round((fatC + fatDiff) / 9.0))
        fatP = Float(round((fatC / calories) * 100))
        caloriesMin = TDEEkcal * goalCaloriesMinValues[goalIndexSettings]
        caloriesMax = TDEEkcal * goalCaloriesMaxValues[goalIndexSettings]
        proteinMin = netWeightStart * goalProteinMinValues[goalIndexSettings]
        proteinMax = netWeightStart * goalProteinMaxValues[goalIndexSettings]
        netCarbsMin = 0.0
        netCarbsMax = 50.0
        
        fatMin = ((caloriesMin - (netCarbs * 4.0)) - (protein * 4.0)) / 9.0
        fatMax = ((caloriesMax - (netCarbs * 4.0)) - (protein * 4.0)) / 9.0
        
    }
        
    func getCaloriesMax() -> Float {
        return caloriesMax
    }
    
    func setCaloriesMax(_ caloriesMax: Float) {
        self.caloriesMax = caloriesMax
    }
    
    func getCaloriesMin() -> Float {
        return caloriesMin
    }
    
    func setCaloriesMin(_ caloriesMin: Float) {
        self.caloriesMin = caloriesMin
    }
    
    func getFatMax() -> Float {
        return fatMax
    }
    
    func setFatMax(_ fatMax: Float) {
        self.fatMax = fatMax
    }
    
    func getFatMin() -> Float {
        return fatMin
    }
    
    func setFatMin(_ fatMin: Float) {
        self.fatMin = fatMin
    }
    
    func getNetCarbsMax() -> Float {
        return netCarbsMax
    }
    
    func setNetCarbsMax(_ netCarbsMax: Float) {
        self.netCarbsMax = netCarbsMax
    }
    
    func getNetCarbsMin() -> Float {
        return netCarbsMin
    }
    
    func setNetCarbsMin(_ netCarbsMin: Float) {
        self.netCarbsMin = netCarbsMin
    }
    
    func getProteinMax() -> Float {
        return proteinMax
    }
    
    func setProteinMax(_ proteinMax: Float) {
        self.proteinMax = proteinMax
    }
    
    func getProteinMin() -> Float {
        return proteinMin
    }
    
    func etProteinMin(_ proteinMin: Float) {
        self.proteinMin = proteinMin
    }
    
    func getCalories() -> Float {
        return calories
    }
    
    func getFat() -> Float {
        return fat
    }
    
    func getNetCarbs() -> Float {
        return netCarbs
    }
    
    func getProtein() -> Float {
        return protein
    }
    
    func getProteinC() -> Float {
        return proteinC
    }
    
    func getFatC() -> Float {
        return fatC
    }
    
    func getNetCarbsP() -> Float {
        return netCarbsP
    }
    
    func getNetCarbsC() -> Float {
        return netCarbsC
    }
    
    func getProteinP() -> Float {
        return proteinP
    }
    
}
