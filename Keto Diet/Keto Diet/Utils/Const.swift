//
//  Const.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit

class Const {
    
    static let AppName                      = "Keto Diet"
    static let AppID                        = "id1277866639"
    static let AppURL                       = "https://itunes.apple.com/us/app/ketogenic-diet-keto-recipes/id1277866639"
    
    // Test ids
//    static let bannerID                     = "ca-app-pub-3940256099942544/6300978111"
//    static let interstitialID               = "ca-app-pub-3940256099942544/1033173712"
    
    // Real ids
    static let bannerID                     = "ca-app-pub-1121147891059674/2634481670"
    static let interstitialID               = "ca-app-pub-1121147891059674/7695236660"
    
    
    
    
    //MARK: - Colors
    static let colorTop                     =  UIColor(red: 255.0/255.0, green: 101.0/255.0, blue: 119/255.0, alpha: 1.0)
    static let colorBottom                  = UIColor(red: 255.0/255.0, green: 126.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    static let nextColor                    = UIColor(red: 255.0/255.0, green: 153.0/255.0, blue: 163.0/255.0, alpha: 1.0)
    
    //MARK: - Labels
    static let maleBodyFats                 = ["4-5%", "6-7%", "8-10%", "11-12%", "13-15%", "16-19%", "20-24%", "25-30%", "35-40%"]
    static let femaleBodyFats               = ["12-14%", "15-17%", "18-20%", "21-23%", "24-26%", "27-29%", "30-35%", "36-40%", "50+%"]
    static let activityLevelTitles          = ["Sedemtary", "Lightly Active", "Moderately Active", "Very Active", "Extreme Active"]
    static let activityLevelContents        = ["Not much daily activity", "Daytime walking, 3-4 hours a week", "Active day job, 3-5 hours a week", "Intense Excercise, 5-6 hours a week", "Training twice a day"]
    static let goalTitles = ["Weight Loss", "Maintain Weight", "Weight Gain"]
    
    static let nutritionItems = ["Foods", "Category", "My Meals", "Recent"]
    
    //MARK: - Int
    static let maleBodyFatValues            = [4, 6, 9, 11, 14, 17, 22, 27, 38]
    static let femaleBodyFatValues          = [13, 16, 19, 22, 25, 28, 32, 38, 50]
    
    
    //MARK: - Images
    static let maleBodyFatImages            = [UIImage(named: "4-5"), UIImage(named: "6-7"), UIImage(named: "8-10"), UIImage(named: "11-12"), UIImage(named: "13-15"), UIImage(named: "16-19"), UIImage(named: "20-24"), UIImage(named: "25-30"), UIImage(named: "35-40")]
    static let femaleBodyFatImages          = [UIImage(named: "12-14"), UIImage(named: "15-17"), UIImage(named: "18-20"), UIImage(named: "21-23"), UIImage(named: "24-26"), UIImage(named: "27-29"), UIImage(named: "30-35"), UIImage(named: "36-40"), UIImage(named: "50+")]
    static let activityLevelImages          = [UIImage(named: "sedentary"), UIImage(named: "lightly-active"), UIImage(named: "moderately-active"),  UIImage(named: "very-active"), UIImage(named: "exterme-active")]
    static let goalImages                   = [UIImage(named: "lose-weight"), UIImage(named: "maintain-weight"), UIImage(named: "gain-weight")]
    
    //MARK: - Int
    static let MALE                         = 1
    static let FEMALE                       = 0
    static let TYPE_WEIGHT_KG               = 1
    static let TYPE_WEIGHT_LBS              = 0
    
    static let FROM_BARCODE                 = 111
    static let FROM_FOODLIST                = 222
    static let FROM_HOME                    = 333
    static let FROM_RECENT                  = 444
    static let FROM_COPY                    = 555
    static let FROM_DATE                    = 666
    static let FROM_CREATEMEAL              = 777
      
    
    
    // Parameters
    static let PARAM_Q                      = "q"
    static let PARAM_FOOD                   = "food"
    static let PARAM_BARCODE                = "barcode"
    
    static let PARAM_FOODS                  = "foods"
    static let PARAM_FOOD_ID                = "food_id"
    static let PARAM_FOOD_NAME              = "food_name"
    static let PARAM_FOOD_TYPE              = "food_type"
    static let PARAM_FOOD_URL               = "food_url"
    static let PARAM_FOOD_DESCRIPTION       = "food_description"
    
    static let PARAM_SUGGESTIONS            = "suggestions"
    static let PARAM_SUGGESTION             = "suggestion"
    
    static let PARAM_SERVINGS               = "servings"
    static let PARAM_SERVING                = "serving"
    
    static let PARAM_CALCIUM                = "calcium"
    static let PARAM_CALORIES               = "calories"
    static let PARAM_CARBOHYDRATE           = "carbohydrate"
    static let PARAM_CHOLESTEROL            = "cholesterol"
    static let PARAM_FAT                    = "fat"
    static let PARAM_FIBER                  = "fiber"
    static let PARAM_IRON                   = "iron"
    static let PARAM_MEASUREMENT_DES        = "measurement_description"
    static let PARAM_METRIC_SERVING_AMOUNT  = "metric_serving_amount"
    static let PARAM_METRIC_SERVING_UNIT    = "metric_serving_unit"
    static let PARAM_MONOUNSATURATED_FAT    = "monounsaturated_fat"
    static let PARAM_NUMBER_OF_UNIT         = "number_of_units"
    static let PARAM_POLYUNSATURATED_FAT    = "polyunsaturated_fat"
    static let PARAM_POSTASSIUM             = "potassium"
    static let PARAM_PROTEIN                = "protein"
    static let PARAM_SATURATED_FAT          = "saturated_fat"
    static let PARAM_SERVING_DES            = "serving_description"
    static let PARAM_SERVING_ID             = "serving_id"
    static let PARAM_SERVING_URL            = "serving_url"
    static let PARAM_SODIUM                 = "sodium"
    static let PARAM_SUGAR                  = "sugar"
    static let PARAM_VITAMIN_A              = "vitamin_a"
    static let PARAM_VITAMIN_C              = "vitamin_c"

    static let PARAM_CATEGORIES             = "categories"
    static let PARAM_VALUE                  = "value"

    
    //MARK: - Keys
    // Key
    static let KEY_USERID                   = "k_userid"
    static let KEY_GENDER                   = "k_gender"
    static let KEY_FAT                      = "k_fat"
    static let KEY_GOAL                     = "k_goal"
    static let KEY_ACTIVITYLEVEL            = "k_acitivity_level"
    static let KEY_WEIGHT                   = "k_weight"
    static let KEY_WEIGHTTYPE               = "k_weighttype"
    static let KEY_ISSTARTED                = "k_isstarted"
    
    static let KEY_N_CALORIES               = "n_calories"
    static let KEY_N_FAT                    = "n_fat"
    static let KEY_N_CARBS                  = "n_carb"
    static let KEY_N_PROTEIN                = "n_protein"
    static let KEY_N_FIBER                  = "n_fiber"
    
    static let KEY_VIEWED                   = "k_viewed"
    static let KEY_HISTORY                  = "k_history"
    static let KEY_RECENT                   = "k_recent"
    static let KEY_MYMEAL                   = "k_mymeal"
    
    static let KEY_ISPURCHASED              = "k_purchased"
    
    // Result code
    static let RESULT_CODE                  = "result_code"
    static let RESULT_SUCCESS               = "result_success"
    static let CODE_FAIL                    = -100
    static let CODE_SUCCESS                 = 200
    static let CODE_105                     = 105
    
    
    // Messages
    static let MSG_ERROR                    = "No internet connection"
    static let MSG_NOTFOUND                 = "Food is not found"
    static let MSG_NOFOOD                   = "Selected day does not have foods"
    static let MSG_COPY_CONFIRM             = "Are you sure want to copy to today?\nCurrent today data will be eraser"
    static let MSG_DELETE                   = "Are you sure want to delete?"
    static let MSG_RESET_GOAL               = "Your all date will be erasered, still want to reset?"
    static let MSG_REMOVE_ADS               = "Would you like to remove ads?"
    
    
}
