//
//  ParseHelper.swift
//  Keto Diet
//
//  Created by Developer on 10/23/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import SwiftyJSON


class ParseHelper {
    
    static func parseFood(_ json: JSON) -> FoodModel {
        
        let food = FoodModel()

        food.id = json[Const.PARAM_FOOD_ID].stringValue
        food.description = json[Const.PARAM_FOOD_DESCRIPTION].stringValue
        food.name = json[Const.PARAM_FOOD_NAME].stringValue
        food.type = json[Const.PARAM_FOOD_TYPE].stringValue
        food.url = json[Const.PARAM_FOOD_URL].stringValue
        
        return food
    }
    
    static func parseFoodServings(_ json: JSON) -> FoodModel {
        
        let food = FoodModel()
        
        food.id = json[Const.PARAM_FOOD][Const.PARAM_FOOD_ID].stringValue
        food.name = json[Const.PARAM_FOOD][Const.PARAM_FOOD_NAME].stringValue
        food.type = json[Const.PARAM_FOOD][Const.PARAM_FOOD_TYPE].stringValue
        food.url = json[Const.PARAM_FOOD][Const.PARAM_FOOD_URL].stringValue
        
        let foodServingsObj = json[Const.PARAM_FOOD][Const.PARAM_SERVINGS][Const.PARAM_SERVING].arrayValue
        
        var servings = [ServingModel]()
       
        for servingObj in foodServingsObj {
            
            let serving = ServingModel()
            
            serving.calcium = servingObj[Const.PARAM_CALCIUM].intValue
            serving.calories = servingObj[Const.PARAM_CALORIES].intValue
            serving.carbohydrate = servingObj[Const.PARAM_CARBOHYDRATE].floatValue
            serving.cholesterol = servingObj[Const.PARAM_CHOLESTEROL].intValue
            serving.fat = servingObj[Const.PARAM_FAT].floatValue
            serving.fiber = servingObj[Const.PARAM_FIBER].floatValue
            serving.iron = servingObj[Const.PARAM_IRON].intValue
            serving.measurement_description = servingObj[Const.PARAM_MEASUREMENT_DES].stringValue
            serving.metric_serving_amount = servingObj[Const.PARAM_METRIC_SERVING_AMOUNT].floatValue
            serving.metric_serving_unit = servingObj[Const.PARAM_METRIC_SERVING_UNIT].stringValue
            serving.monounsaturated_fat = servingObj[Const.PARAM_MONOUNSATURATED_FAT].floatValue
            serving.number_of_units = servingObj[Const.PARAM_FAT].floatValue
            serving.polyunsaturated_fat = servingObj[Const.PARAM_POLYUNSATURATED_FAT].floatValue
            serving.potassium = servingObj[Const.PARAM_IRON].intValue
            serving.protein = servingObj[Const.PARAM_PROTEIN].floatValue
            serving.saturated_fat = servingObj[Const.PARAM_SATURATED_FAT].floatValue
            serving.serving_description = servingObj[Const.PARAM_SERVING_DES].stringValue
            serving.serving_id = servingObj[Const.PARAM_SERVING_ID].intValue
            serving.serving_url = servingObj[Const.PARAM_SERVING_URL].stringValue
            serving.sodium = servingObj[Const.PARAM_SODIUM].intValue
            serving.sugar = servingObj[Const.PARAM_SUGAR].floatValue
            serving.vitamin_a = servingObj[Const.PARAM_VITAMIN_A].intValue
            serving.vitamin_c = servingObj[Const.PARAM_VITAMIN_C].intValue
            
            servings.append(serving)            
        }
        
        food.servings = servings
        
        
        return food
    }
    
    static func parseFoodServingOne(_ json: JSON) -> FoodModel {
        
        let food = FoodModel()
        
        food.id = json[Const.PARAM_FOOD][Const.PARAM_FOOD_ID].stringValue
        food.name = json[Const.PARAM_FOOD][Const.PARAM_FOOD_NAME].stringValue
        food.type = json[Const.PARAM_FOOD][Const.PARAM_FOOD_TYPE].stringValue
        food.url = json[Const.PARAM_FOOD][Const.PARAM_FOOD_URL].stringValue
        
        let servingObj = json[Const.PARAM_FOOD][Const.PARAM_SERVINGS][Const.PARAM_SERVING]
        
        var servings = [ServingModel]()
        
        let serving = ServingModel()
        
        serving.calcium = servingObj[Const.PARAM_CALCIUM].intValue
        serving.calories = servingObj[Const.PARAM_CALORIES].intValue
        serving.carbohydrate = servingObj[Const.PARAM_CARBOHYDRATE].floatValue
        serving.cholesterol = servingObj[Const.PARAM_CHOLESTEROL].intValue
        serving.fat = servingObj[Const.PARAM_FAT].floatValue
        serving.fiber = servingObj[Const.PARAM_FIBER].floatValue
        serving.iron = servingObj[Const.PARAM_IRON].intValue
        serving.measurement_description = servingObj[Const.PARAM_MEASUREMENT_DES].stringValue
        serving.metric_serving_amount = servingObj[Const.PARAM_METRIC_SERVING_AMOUNT].floatValue
        serving.metric_serving_unit = servingObj[Const.PARAM_METRIC_SERVING_UNIT].stringValue
        serving.monounsaturated_fat = servingObj[Const.PARAM_MONOUNSATURATED_FAT].floatValue
        serving.number_of_units = servingObj[Const.PARAM_FAT].floatValue
        serving.polyunsaturated_fat = servingObj[Const.PARAM_POLYUNSATURATED_FAT].floatValue
        serving.potassium = servingObj[Const.PARAM_IRON].intValue
        serving.protein = servingObj[Const.PARAM_PROTEIN].floatValue
        serving.saturated_fat = servingObj[Const.PARAM_SATURATED_FAT].floatValue
        serving.serving_description = servingObj[Const.PARAM_SERVING_DES].stringValue
        serving.serving_id = servingObj[Const.PARAM_SERVING_ID].intValue
        serving.serving_url = servingObj[Const.PARAM_SERVING_URL].stringValue
        serving.sodium = servingObj[Const.PARAM_SODIUM].intValue
        serving.sugar = servingObj[Const.PARAM_SUGAR].floatValue
        serving.vitamin_a = servingObj[Const.PARAM_VITAMIN_A].intValue
        serving.vitamin_c = servingObj[Const.PARAM_VITAMIN_C].intValue
        
        servings.append(serving)
      
        food.servings = servings
        
        return food
    }
}
