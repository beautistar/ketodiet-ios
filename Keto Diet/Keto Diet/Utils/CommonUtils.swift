//
//  CommonUtils.swift
//  Keto Diet
//
//  Created by Alex on 2018/10/12.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation
import UIKit


var userDefaults = UserDefaults.standard
var user = UserModel()
var HistoryList = [HistoryModel]()
var RecentHistoryList = [HistoryModel]()
var MyMealList = [MealModel]()

var foodAddCount = 0
var interstitialCount = 0

var ENABLE_NETCARB: Bool = true
var SelectedDay: Date = Date()


var currentUser : UserModel? {
    didSet {
        if let user = currentUser {
            userDefaults.set(user.gender, forKey: Const.KEY_GENDER)
            userDefaults.set(user.weight, forKey: Const.KEY_WEIGHT)
            userDefaults.set(user.goal, forKey: Const.KEY_GOAL)
            userDefaults.set(user.weightType, forKey: Const.KEY_WEIGHTTYPE)
            userDefaults.set(user.activityLevel, forKey: Const.KEY_ACTIVITYLEVEL)
            userDefaults.set(user.fat, forKey: Const.KEY_FAT)
            userDefaults.set(user.fat, forKey: Const.KEY_ISSTARTED)
        }
    }
}

func getCurrentUser() -> UserModel {
    
    let user = UserModel()
    
    user.gender = userDefaults.integer(forKey: Const.KEY_GENDER)
    user.weight = userDefaults.float(forKey: Const.KEY_WEIGHT)
    user.weightType = userDefaults.integer(forKey: Const.KEY_WEIGHTTYPE)
    user.goal = userDefaults.integer(forKey: Const.KEY_GOAL)
    user.activityLevel = userDefaults.integer(forKey: Const.KEY_ACTIVITYLEVEL)
    user.fat = userDefaults.float(forKey: Const.KEY_FAT)
    user.isStarted = userDefaults.bool(forKey: Const.KEY_ISSTARTED)
    return user
}

var currentNutrition : NutritionModel? {
    didSet {
        if let nutrition = currentNutrition {
            
            userDefaults.set(nutrition.calories, forKey: Const.KEY_N_CALORIES)
            userDefaults.set(nutrition.fat, forKey: Const.KEY_N_FAT)
            userDefaults.set(nutrition.carbs, forKey: Const.KEY_N_CARBS)
            userDefaults.set(nutrition.protein, forKey: Const.KEY_N_PROTEIN)
            userDefaults.set(nutrition.fiber, forKey: Const.KEY_N_FIBER)
        }
    }
}

func getcurrentNutrition() -> NutritionModel {
    
    let nutrition = NutritionModel()
    
    nutrition.calories = userDefaults.float(forKey: Const.KEY_N_CALORIES)
    nutrition.fat = userDefaults.float(forKey: Const.KEY_N_FAT)
    nutrition.carbs = userDefaults.float(forKey: Const.KEY_N_CARBS)
    nutrition.protein = userDefaults.float(forKey: Const.KEY_N_PROTEIN)
    nutrition.fiber = userDefaults.float(forKey: Const.KEY_N_FIBER)
    
    
    return nutrition
}

func getHistory() -> [HistoryModel] {
    
    if let data = userDefaults.data(forKey: Const.KEY_HISTORY),
        let history = NSKeyedUnarchiver.unarchiveObject(with: data) as? [HistoryModel] {
        return history
    }
    
    
    return []
    
}

func saveHistory(_ histories: [HistoryModel]) {
    
    let encodedNewData: Data = NSKeyedArchiver.archivedData(withRootObject: histories)
    userDefaults.set(encodedNewData, forKey: Const.KEY_HISTORY)
    userDefaults.synchronize()
}

func resetHistory() {
    
    let histories = [HistoryModel]()
    let encodedNewData: Data = NSKeyedArchiver.archivedData(withRootObject: histories)
    userDefaults.set(encodedNewData, forKey: Const.KEY_HISTORY)
    userDefaults.synchronize()
}

func getRecent() -> [HistoryModel] {
    
    if let data = userDefaults.data(forKey: Const.KEY_RECENT) ,
        let recentList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [HistoryModel] {
        return recentList
    }
    
    return []
}

func saveRecent(_ recent: [HistoryModel]) {
    
    
}

func getMyMeal() -> [MealModel] {
    
    if let data = userDefaults.data(forKey: Const.KEY_MYMEAL) ,
        let mealList = NSKeyedUnarchiver.unarchiveObject(with: data) as? [MealModel] {
        return mealList
    }
    
    return []
}

func saveMyMeals(_ meals: [MealModel]) {    
    
    let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: meals)
    userDefaults.set(encodedData, forKey: Const.KEY_MYMEAL)
    userDefaults.synchronize()
    
    
}

func addHistory(_ history: HistoryModel) {    
    
}

class CommonUtils {

    
    
}



