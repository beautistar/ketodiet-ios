//
//  ObjectiveCell.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class ObjectiveCell: UITableViewCell {

    @IBOutlet weak var imvObjective: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        if isSelected {
            self.backgroundColor = Const.nextColor
        } else {
            self.backgroundColor = UIColor.clear
        }
    }

}
