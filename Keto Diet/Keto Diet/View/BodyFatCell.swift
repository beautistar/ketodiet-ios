//
//  BodyFatCell.swift
//  Keto Diet
//
//  Created by Developer on 10/14/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class BodyFatCell: UICollectionViewCell {
    
    @IBOutlet weak var imvBody: UIImageView!
    @IBOutlet weak var lblFat: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        borderView.layer.borderWidth = 3.0
        borderView.layer.borderColor = Const.nextColor.cgColor
        
    }   
    
}
