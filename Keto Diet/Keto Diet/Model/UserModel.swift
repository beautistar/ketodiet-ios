//
//  UserModel.swift
//  Keto Diet
//
//  Created by Developer on 10/21/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class UserModel {
    
    var userId: Int = 0
    var fat: Float = 0.0                // Fat percent
    var goal: Int = 0                   // Goal index
    var activityLevel: Int = 0          // Activiry level index
    var weight: Float = 0.0             // Weight
    var weightType: Int = 0             // weight type (Kg or lbs)
    var gender: Int = 1                 // Male: 1, Female: 0
    
    var isStarted: Bool = false
}
