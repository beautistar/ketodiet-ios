//
//  ServingModel.swift
//  Keto Diet
//
//  Created by Developer on 10/23/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class ServingModel {
    
    var calcium = 0
    var calories = 0
    var carbohydrate: Float = 0.0
    var cholesterol = 0
    var fat: Float = 0.0
    var fiber: Float = 0.0
    var iron = 0
    var potassium = 0
    var serving_id = 0
    var sodium = 0
    var vitamin_a = 0
    var vitamin_c = 0
    var metric_serving_amount: Float = 0.0
    var monounsaturated_fat: Float = 0.0
    var number_of_units: Float = 0.0
    var polyunsaturated_fat: Float = 0.0
    var protein: Float = 0.0
    var saturated_fat: Float = 0.0
    var measurement_description = ""
    var metric_serving_unit = ""
    var serving_description = ""
    var serving_url = ""
    var sugar: Float = 0.0
    
}
