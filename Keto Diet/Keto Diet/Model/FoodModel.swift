//
//  FoodModel.swift
//  Keto Diet
//
//  Created by Developer on 10/23/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class FoodModel {
    
    var description = ""
    var id = ""
    var name = ""
    var type = ""
    var url = ""
    var servings = [ServingModel]()
}
