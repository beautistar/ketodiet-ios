//
//  HistoryModel.swift
//  Keto Diet
//
//  Created by Developer on 10/31/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class HistoryModel: NSObject, NSCoding {

    var foodId: String = ""
    var foodName: String = ""
    var carbs: Float = 0.0
    var calories: Int = 0
    var fat: Float = 0.0
    var protein: Float = 0.0
    var addDate: Date = Date()
    
    init(foodId: String, foodName: String, carbs: Float, calories: Int, fat: Float, protein: Float, addDate: Date) {
    
        self.foodId = foodId
        self.foodName = foodName
        self.carbs = carbs
        self.calories = calories
        self.fat = fat
        self.protein = protein
        self.addDate = addDate
        
    }
    
    
    func encode(with aCoder: NSCoder) {
    
        aCoder.encode(foodId, forKey: "h_foodid")
        aCoder.encode(foodName, forKey: "h_foodname")
        aCoder.encode(carbs, forKey: "h_carbs")
        aCoder.encode(calories, forKey: "h_calories")
        aCoder.encode(fat, forKey: "h_fat")
        aCoder.encode(protein, forKey: "h_protein")
        aCoder.encode(addDate, forKey: "h_adddate")
    }
    
    
    required convenience init(coder aDecoder: NSCoder) {

        
        let foodId = aDecoder.decodeObject(forKey: "h_foodid") as! String
        let foodName = aDecoder.decodeObject(forKey: "h_foodname") as! String
        let carbs = aDecoder.decodeFloat(forKey: "h_carbs")
        let calories = aDecoder.decodeInteger(forKey: "h_calories")
        let fat = aDecoder.decodeFloat(forKey: "h_fat")
        let protein = aDecoder.decodeFloat(forKey: "h_protein")
        let addDate = aDecoder.decodeObject(forKey: "h_adddate") as! Date

        
        self.init(foodId: foodId, foodName: foodName, carbs: carbs, calories: calories, fat: fat, protein: protein, addDate: addDate)
        
    }
    
}
