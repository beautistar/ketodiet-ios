//
//  MealModel.swift
//  Keto Diet
//
//  Created by Developer on 11/8/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import UIKit

class MealModel: NSObject, NSCoding {

    var id: String = ""
    var name: String = ""
    var nutrition = [HistoryModel]()
    
    init(name: String, nutrition: [HistoryModel]) {
        
        self.name = name
        self.nutrition = nutrition
        self.id = String(format: "%@_%.1f_%.1f_%.1f_%.1f", name.trimmingCharacters(in: .whitespaces),
                         nutrition.map( {$0.carbs}).reduce(0, +),
                         Float(nutrition.map( {$0.calories}).reduce(0, +)),
                         nutrition.map( {$0.fat}).reduce(0, +),
                         nutrition.map( {$0.protein}).reduce(0, +)
                        )
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(id, forKey: "m_id")
        aCoder.encode(name, forKey: "m_name")
        aCoder.encode(nutrition, forKey: "m_nutrition")
//        aCoder.encode(nutrition.calories, forKey: "m_calories")
//        aCoder.encode(nutrition.carbs, forKey: "m_carbs")
//        aCoder.encode(nutrition.fat, forKey: "m_fat")
//        aCoder.encode(nutrition.protein, forKey: "m_protein")
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        
        //let id = aDecoder.decodeObject(forKey: "m_id") as! String
        let name = aDecoder.decodeObject(forKey: "m_name") as! String
        let nutrition = aDecoder.decodeObject(forKey: "m_nutrition") as! [HistoryModel]
//        nutrition.calories = aDecoder.decodeFloat(forKey: "m_calories")
//        nutrition.carbs = aDecoder.decodeFloat(forKey: "m_carbs")
//        nutrition.protein = aDecoder.decodeFloat(forKey: "m_protein")
//        nutrition.fat = aDecoder.decodeFloat(forKey: "m_fat")
        
        self.init(name: name, nutrition: nutrition)
    }
    

}
