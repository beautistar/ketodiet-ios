//
//  NutritionModel.swift
//  Keto Diet
//
//  Created by Developer on 10/28/18.
//  Copyright © 2018 Alex. All rights reserved.
//

import Foundation

class NutritionModel {
    
    var calories: Float = 0.0
    var carbs: Float = 0.0
    var protein: Float = 0.0
    var fat: Float = 0.0
    var fiber: Float = 0.0
}
